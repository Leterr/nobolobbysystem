package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.util.ItemStackManager;
import net.noboplay.lobbysystem.util.Profil;
import net.noboplay.lobbysystem.util.Shop;
import net.noboplay.lobbysystem.Variables;
import net.noboplay.lobbysystem.boots.Boots;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class ShopManager implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        try {
            if (event.getInventory().getName().equalsIgnoreCase(ChatColor.GREEN + "Shop")) {
                if (event.getCurrentItem() != null) {
                    event.setCancelled(true);
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Pets")) {
                        player.closeInventory();
                        Shop.openPets(player);
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "K" + Variables.CHAR_OE + "pfe")) {
                        player.closeInventory();
                        if (player.hasPermission("server.premium") || player.hasPermission("server.youtuber")
                                || player.hasPermission("server.premium+") || player.hasPermission("server.team")) {
                            Shop.openHeads(player);
                        } else {
                            player.sendMessage(LobbySystem.getInstance().PREFIX
                                    + ChatColor.GOLD + "Dieses Feature ist bislang nur f" + Variables.CHAR_UE + "r Premium-User verf" + Variables.CHAR_UE + "gbar.");
                        }
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Boots")) {
                        player.closeInventory();
                        Boots.openShop(player);
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "Zur" + Variables.CHAR_UE + "ck")) {
                        player.closeInventory();
                        Profil.openProfile(player);
                    }
                }
            } else if (event.getInventory().getName().equalsIgnoreCase(ChatColor.AQUA + "K" + Variables.CHAR_OE + "pfe")) {
                if (event.getCurrentItem() != null) {
                    event.setCancelled(true);
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Noboplays")) {
                        player.getInventory()
                                .setHelmet(ItemStackManager.createHead(ChatColor.DARK_RED + "Noboplays", null, "Noboplays"));
                        player.sendMessage(
                                LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast nun den Kopf von " + ChatColor.DARK_RED + "Noboplays " + ChatColor.GREEN + "auf.");
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Zocker_SK")) {
                        player.getInventory()
                                .setHelmet(ItemStackManager.createHead(ChatColor.DARK_RED + "Zocker_SK", null, "Zocker_SK"));
                        player.sendMessage(
                                LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast nun den Kopf von " + ChatColor.DARK_RED + "Zocker_SK " + ChatColor.GREEN + "auf.");
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Leterr")) {
                        player.getInventory().setHelmet(ItemStackManager.createHead(ChatColor.DARK_RED + "Leterr", null, "Leterr"));
                        player.sendMessage(
                                LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast nun den Kopf von " + ChatColor.DARK_RED + "Leterr " + ChatColor.GREEN + "auf.");
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "MichiVIP")) {
                        player.getInventory().setHelmet(ItemStackManager.createHead(ChatColor.DARK_RED + "MichiVIP", null, "MichiVIP"));
                        player.sendMessage(
                                LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast nun den Kopf von " + ChatColor.DARK_RED + "MichiVIP " + ChatColor.GREEN + "auf.");
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "DevCoffee")) {
                        player.getInventory().setHelmet(ItemStackManager.createHead(ChatColor.DARK_RED + "DevCoffee", null, "DevCoffee"));
                        player.sendMessage(
                                LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast nun den Kopf von " + ChatColor.DARK_RED + "DevCoffee " + ChatColor.GREEN + "auf.");
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GRAY + "Zur" + Variables.CHAR_UE + "ck")) {
                        player.closeInventory();
                        Shop.openShop(player);
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.RED + "Kopf entfernen")) {
                        player.getInventory().setHelmet(new ItemStack(Material.AIR));
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast deinen Kopf entfernt.");
                    }
                }
            }
        } catch (Exception exception) {
        }
    }
}
