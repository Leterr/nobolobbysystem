package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.util.*;
import net.noboplay.lobbysystem.util.Settings.SettingsOption;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ProfilManager implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getItem() != null) {
            if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "Dein Profil")) {
                Profil.openProfile(player);
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getName().equalsIgnoreCase(ChatColor.GREEN + "Dein Profil")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Spieler verstecken")) {
                player.closeInventory();
                if (Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.HIDDEN)) {
                    Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.HIDDEN, false);
                    Bukkit.getOnlinePlayers().forEach(player::showPlayer);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 1));
                    player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 60);
                    player.sendMessage(
                            LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast alle Spieler sichtbar geschalten.");
                } else {
                    Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.HIDDEN, true);
                    Bukkit.getOnlinePlayers().stream().filter(players -> !players.hasPermission("server.team")).forEach(player::hidePlayer);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 1));
                    player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 60);
                    player.sendMessage(LobbySystem.getInstance().PREFIX
                            + ChatColor.RED + "Du hast alle Spieler ohne einem Teamrang versteckt.");
                }
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_AQUA + "Einstellungen")) {
                player.closeInventory();
                Config.openConfig(player);
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Shop")) {
                player.closeInventory();
                Shop.openShop(player);
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + "Freunde")) {
                player.closeInventory();
                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Die Freunde werden vorraussichtlich im April erscheinen.");
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Gadgets")) {
                player.closeInventory();
                Gadgets.openGadgets(player);
            }
        }
    }

}
