package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.util.InventoryUtil;
import net.noboplay.lobbysystem.util.ItemStackManager;
import net.noboplay.lobbysystem.util.Settings;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;
import java.util.List;

public class ClothingManagerNew implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getName().contains(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Kleiderschrank")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getType() == Material.LEATHER_HELMET) {
                player.getInventory().setHelmet(event.getCurrentItem());
            } else if (event.getCurrentItem().getType() == Material.LEATHER_CHESTPLATE) {
                player.getInventory().setChestplate(event.getCurrentItem());
            } else if (event.getCurrentItem().getType() == Material.LEATHER_LEGGINGS) {
                player.getInventory().setLeggings(event.getCurrentItem());
            } else if (event.getCurrentItem().getType() == Material.LEATHER_BOOTS) {
                player.getInventory().setBoots(event.getCurrentItem());
            } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Entfernen")) {
                player.getInventory().setArmorContents(null);
                player.updateInventory();
                player.closeInventory();
            } else if (event.getCurrentItem().getItemMeta().getDisplayName().startsWith(ChatColor.AQUA + "N" + Variables.CHAR_AE + "chste Seite")) {
                int site = Integer.parseInt(event.getCurrentItem().getItemMeta().getDisplayName().split("(")[1].substring(0, event.getCurrentItem().getItemMeta().getDisplayName().split("(")[1].length() - 1));
            }
            Settings.setArmor(player.getUniqueId().toString(),
                    InventoryUtil.itemStackArrayToBase64(player.getInventory().getArmorContents()));
        }
    }

    public static void openKleiderSchrank(Player player) {
        Inventory inv = Bukkit.createInventory(null, 5 * 9, ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Kleiderschrank");
        ItemStack remove = ItemStackManager.createItem(ChatColor.DARK_RED + "Entfernen", ChatColor.GRAY + "Entferne deine Kleidung",
                Material.BARRIER, 0, 1);

        setItemInv(inv, 0, Color.RED);
        setItemInv(inv, 1, Color.BLACK);
        setItemInv(inv, 2, Color.LIME);
        setItemInv(inv, 3, Color.NAVY);
        setItemInv(inv, 4, Color.PURPLE);
        setItemInv(inv, 5, Color.GRAY);
        setItemInv(inv, 6, Color.ORANGE);
        setItemInv(inv, 7, Color.WHITE);
        setItemInv(inv, 8, Color.BLUE);

        inv.setItem(44, remove);

        player.openInventory(inv);
    }

    private static void setItemInv(Inventory inv, int slot, Color c) {

        inv.setItem(slot, createItem(Material.LEATHER_HELMET, c, ChatColor.AQUA + "Helm"));
        inv.setItem(slot + 9, createItem(Material.LEATHER_CHESTPLATE, c, ChatColor.AQUA + "Brustpanzer"));
        inv.setItem(slot + 9 + 9, createItem(Material.LEATHER_LEGGINGS, c, ChatColor.AQUA + "Hose"));
        inv.setItem(slot + 9 + 9 + 9, createItem(Material.LEATHER_BOOTS, c, ChatColor.AQUA + "Schuhe"));

    }

    private static ItemStack createItem(Material m, Color c, String name) {

        ItemStack is = new ItemStack(m);
        LeatherArmorMeta im = (LeatherArmorMeta) is.getItemMeta();
        im.setDisplayName(name);
        im.setColor(c);
        is.setItemMeta(im);
        return is;
    }

    public enum ColorCodes {
        BLACK(0, 0, 0, ChatColor.BLACK + "Schwarz"),
        DARKBLUE(0, 0, 170, ChatColor.DARK_BLUE + "Dunkelblau"),
        DARKGREEN(0, 170, 0, ChatColor.DARK_GREEN + "Dunkelgr" + Variables.CHAR_UE + "n"),
        DARKAQUA(0, 170, 170, ChatColor.DARK_AQUA + "dunkles T" + Variables.CHAR_UE + "rkis"),
        DARKRED(170, 0, 0, ChatColor.RED + "Dunkelrot"),
        DARKPURPLE(170, 0, 170, ChatColor.DARK_PURPLE + "Violett"),
        GOLD(255, 170, 0, ChatColor.GOLD + "Orange"),
        GRAY(170, 170, 170, ChatColor.GRAY + "Grau"),
        DARKGRAY(85, 85, 85, ChatColor.DARK_GRAY + "Dunkelgrau"),
        BLUE(85, 85, 255, ChatColor.BLUE + "Blau"),
        GREEN(85, 255, 85, ChatColor.GREEN + "Gr" + Variables.CHAR_UE + "n"),
        AQUA(85, 255, 255, ChatColor.AQUA + "T" + Variables.CHAR_UE + "rkis"),
        RED(255, 85, 85, ChatColor.RED + "Rot"),
        LIGHTPURPLE(255, 85, 255, ChatColor.LIGHT_PURPLE + "Rosa"),
        YELLOW(255, 255, 85, ChatColor.YELLOW + "Gelb"),
        WHITE(255, 255, 255, ChatColor.WHITE + "Wei" + Variables.CHAR_SZ),
        OTHERCOLOR1(42, 203, 169, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR2(127, 95, 208, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR3(37, 141, 180, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR4(54, 213, 103, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR5(59, 179, 200, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR6(35, 102, 190, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR7(82, 35, 190, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR8(29, 149, 59, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR9(119, 54, 177, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR10(236, 48, 200, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e"),
        OTHERCOLOR11(54, 203, 189, ChatColor.getByChar('1') + "S" + ChatColor.getByChar('2') + "p" + ChatColor.getByChar('3') + "e" + ChatColor.getByChar('4') + "z" + ChatColor.getByChar('5') + "i" + ChatColor.getByChar('6') + "a" + ChatColor.getByChar('7') + "l" + ChatColor.getByChar('a') + "f" + ChatColor.getByChar('b') + "a" + ChatColor.getByChar('c') + "r" + ChatColor.getByChar('d') + "b" + ChatColor.getByChar('e') + "e");

        private int r, g, b;
        private String name;

        private ColorCodes(int r, int g, int b, String name) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.name = name;
        }

        public int getR() {
            return r;
        }

        public int getG() {
            return g;
        }

        public int getB() {
            return b;
        }

        public String getName() {
            return name;
        }

        public static List<ColorCodes> getColors() {
            List<ColorCodes> units = new ArrayList<>();
            for (ColorCodes unit : ColorCodes.values()) {
                units.add(unit);
            }
            return units;
        }

        public static ColorCodes findColor(String displayname) {
            for (ColorCodes codes : getColors()) {
                if (codes.getName().equals(displayname)) {
                    return codes;
                }
            }
            return null;
        }

    }

}