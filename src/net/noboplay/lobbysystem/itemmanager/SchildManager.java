package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.core.api.Core;
import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class SchildManager implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        try {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + "Schutzschild")) {
                    if (Core.getServerName().toLowerCase().startsWith("silenthub")) {
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GRAY + "Nur im Hub verf" + Variables.CHAR_UE + "gbar. /hub");
                        return;
                    }
                    event.setCancelled(true);
                    if (!LobbySystem.getInstance().schild.containsKey(player.getName())) {
                        LobbySystem.getInstance().schild.put(player.getName(), new BukkitRunnable() {

                            @Override
                            public void run() {

                                player.getWorld().playEffect(player.getLocation(), Effect.ENDER_SIGNAL, 1);

                                for (Entity entity : player.getNearbyEntities(2, 2, 2)) {
                                    if (entity instanceof Player) {
                                        Player target = (Player) entity;

                                        double ax = player.getLocation().getX();
                                        double ay = player.getLocation().getY();
                                        double az = player.getLocation().getZ();

                                        double bx = target.getLocation().getX();
                                        double by = target.getLocation().getY();
                                        double bz = target.getLocation().getZ();

                                        double x = bx - ax;
                                        double y = by - ay;
                                        double z = bz - az;

                                        Vector vector = new Vector(x, y, z).normalize().multiply(1.4D).setY(0.6D);

                                        if (!target.hasPermission("server.schild")) {
                                            target.setVelocity(vector);
                                        }
                                    }
                                }

                            }
                        });
                        LobbySystem.getInstance().schild.get(player.getName())
                                .runTaskTimerAsynchronously(LobbySystem.getInstance(), 0, 2);

                        player.sendMessage(
                                LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Du hast das " + ChatColor.DARK_PURPLE + "Schutzschild " + ChatColor.DARK_AQUA + "aktiviert!");
                        return;
                    }
                    if (LobbySystem.getInstance().schild.containsKey(player.getName())) {
                        LobbySystem.getInstance().schild.get(player.getName());
                        LobbySystem.getInstance().schild.get(player.getName()).cancel();
                        LobbySystem.getInstance().schild.remove(player.getName());

                        player.sendMessage(
                                LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Du hast das " + ChatColor.DARK_PURPLE + "Schutzschild " + ChatColor.DARK_AQUA + "deaktiviert!");
                    }
                }
            }
        } catch (Exception exception) {
        }
    }

}
