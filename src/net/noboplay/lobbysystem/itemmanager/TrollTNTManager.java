package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.LobbySystem;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class TrollTNTManager implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        try {
            Player player = event.getPlayer();
            Vector vector = player.getLocation().getDirection().multiply(0.7D).setY(0.2D);
            if ((event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)
                    && event.getItem() != null) {
                if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Troll-TNT")) {
                    if (LobbySystem.getInstance().tnt.getIfPresent(player.getName()) != null) {
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Bitte warte ein bisschen...");
                        return;
                    }
                    LobbySystem.getInstance().tnt.put(player.getName(), player.getName());
                    TNTPrimed tnt = player.getWorld().spawn(player.getEyeLocation(), TNTPrimed.class);
                    tnt.setVelocity(vector);
                    tnt.setFireTicks(15);
                }
            }
        } catch (Exception exception) {
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamage(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            if (event.getDamager() instanceof TNTPrimed) {
                Player player = (Player) event.getEntity();
                float x = (float) -1 + (float) (Math.random() * ((5 - -1) + 1));
                float y = (float) -5 + (float) (Math.random() * ((5 - -5) + 1));
                float z = (float) -0.3 + (float) (Math.random() * ((6.3 - -0.3) + 1));
                player.setVelocity(new Vector(x, y, z));
            }
        }
    }
}
