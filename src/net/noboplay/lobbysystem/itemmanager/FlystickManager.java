
package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.util.Settings;
import net.noboplay.lobbysystem.util.Settings.SettingsOption;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class FlystickManager implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            try {
                if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Flugstab")) {
                    Player player = event.getPlayer();
                    if (player.getGameMode() != GameMode.CREATIVE
                            & Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.FLYSTICK)) {
                        player.setVelocity(player.getLocation().getDirection().multiply(2.2D).setY(0.6D));
                        player.playSound(player.getLocation(), Sound.ENDERDRAGON_WINGS, 1.0F, 1.0F);
                    }
                }
            } catch (Exception exception) {
            }
        }
        if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            final Player player = event.getPlayer();
            try {
                if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Flugstab")) {
                    if (player.hasPermission("server.premium")) {
                        if (!Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.FLYSTICK)) {
                            player.setGameMode(GameMode.SURVIVAL);
                            Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.FLYSTICK, true);
                            player.sendMessage(
                                    LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Du hast den " + ChatColor.GOLD + "Flugstab " + ChatColor.DARK_AQUA + "aktiviert!");
                            player.setAllowFlight(true);
                            LobbySystem.getInstance().jumper.put(player.getName(), new BukkitRunnable() {

                                @Override
                                public void run() {
                                    player.setFlying(false);
                                }

                            });
                            LobbySystem.getInstance().jumper.get(player.getName())
                                    .runTaskTimer(LobbySystem.getInstance(), 0, 1);
                            return;
                        }
                        if (Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.FLYSTICK)) {
                            Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.FLYSTICK, false);
                            player.setAllowFlight(false);
                            player.setFlying(false);
                            player.sendMessage(
                                    LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Du hast den " + ChatColor.GOLD + "Flugstab " + ChatColor.DARK_AQUA + "deaktiviert!");
                            LobbySystem.getInstance().jumper.get(player.getName()).cancel();
                            LobbySystem.getInstance().jumper.get(player.getName());
                            LobbySystem.getInstance().jumper.remove(player.getName());
                            return;
                        }
                    } else {
                        player.sendMessage(
                                LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Dieses Item ist nur f" + Variables.CHAR_UE + "r " + ChatColor.GOLD + "Premium-Spieler.");
                    }
                }
            } catch (Exception exception) {
            }
        }
    }
}