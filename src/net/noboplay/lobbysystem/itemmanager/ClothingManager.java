package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.util.InventoryUtil;
import net.noboplay.lobbysystem.util.ItemStackManager;
import net.noboplay.lobbysystem.util.Settings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class ClothingManager implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getName().contains(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Kleiderschrank")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getType() == Material.LEATHER_HELMET) {
                player.getInventory().setHelmet(event.getCurrentItem());
            } else if (event.getCurrentItem().getType() == Material.LEATHER_CHESTPLATE) {
                player.getInventory().setChestplate(event.getCurrentItem());
            } else if (event.getCurrentItem().getType() == Material.LEATHER_LEGGINGS) {
                player.getInventory().setLeggings(event.getCurrentItem());
            } else if (event.getCurrentItem().getType() == Material.LEATHER_BOOTS) {
                player.getInventory().setBoots(event.getCurrentItem());
            } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Entfernen")) {
                player.getInventory().setArmorContents(null);
                player.updateInventory();
                player.closeInventory();
            }
            Settings.setArmor(player.getUniqueId().toString(),
                    InventoryUtil.itemStackArrayToBase64(player.getInventory().getArmorContents()));
        }
    }

    public static void openKleiderSchrank(Player player) {
        Inventory inv = Bukkit.createInventory(null, 5 * 9, ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Kleiderschrank");
        ItemStack remove = ItemStackManager.createItem(ChatColor.DARK_RED + "Entfernen", ChatColor.GRAY + "Entferne deine Kleidung",
                Material.BARRIER, 0, 0);

        setItemInv(inv, 0, Color.RED);
        setItemInv(inv, 1, Color.BLACK);
        setItemInv(inv, 2, Color.LIME);
        setItemInv(inv, 3, Color.NAVY);
        setItemInv(inv, 4, Color.PURPLE);
        setItemInv(inv, 5, Color.GRAY);
        setItemInv(inv, 6, Color.ORANGE);
        setItemInv(inv, 7, Color.WHITE);
        setItemInv(inv, 8, Color.BLUE);

        inv.setItem(44, remove);

        player.openInventory(inv);
    }

    private static void setItemInv(Inventory inv, int slot, Color c) {

        inv.setItem(slot, createItem(Material.LEATHER_HELMET, c, ChatColor.AQUA + "Helm"));
        inv.setItem(slot + 9, createItem(Material.LEATHER_CHESTPLATE, c, ChatColor.AQUA + "Brustpanzer"));
        inv.setItem(slot + 9 + 9, createItem(Material.LEATHER_LEGGINGS, c, ChatColor.AQUA + "Hose"));
        inv.setItem(slot + 9 + 9 + 9, createItem(Material.LEATHER_BOOTS, c, ChatColor.AQUA + "Schuhe"));

    }

    private static ItemStack createItem(Material m, Color c, String name) {

        ItemStack is = new ItemStack(m);
        LeatherArmorMeta im = (LeatherArmorMeta) is.getItemMeta();
        im.setDisplayName(name);
        im.setColor(c);
        is.setItemMeta(im);

        return is;
    }

}