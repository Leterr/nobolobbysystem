package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.util.ItemStackManager;
import net.noboplay.lobbysystem.util.Profil;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class GadgetsManager implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Gadgets")) {
            event.setCancelled(true);
            try {
                if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "Kleiderschrank")) {
                    player.closeInventory();
                    ClothingManagerNew.openKleiderSchrank(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Flugstab")) {
                    ItemStack flystick = ItemStackManager.createItem(ChatColor.GOLD + "Flugstab", ChatColor.GRAY + "Fliege wie ein Vogel",
                            Material.BLAZE_POWDER, 1, 0);
                    player.getInventory().setItem(8, flystick);
                    player.updateInventory();
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + "EnderBow")) {
                    ItemStack pearl = ItemStackManager.createItem(ChatColor.DARK_PURPLE + "EnderBow", "", Material.BOW, 1, 0);
                    ItemStackManager.enchantItem(pearl, Enchantment.DURABILITY, 1);
                    ItemStackManager.addItemFlag(pearl, ItemFlag.HIDE_ENCHANTS);
                    player.getInventory().setItem(8, pearl);
                    player.updateInventory();
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Troll-TNT")) {
                    ItemStack tnt = ItemStackManager.createItem(ChatColor.DARK_RED + "Troll-TNT", "", Material.TNT, 1, 0);
                    player.getInventory().setItem(8, tnt);
                    player.updateInventory();
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "Zur" + Variables.CHAR_UE + "ck")) {
                    player.closeInventory();
                    Profil.openProfile(player);
                }
            } catch (Exception exception) {
            }
        }
    }

}