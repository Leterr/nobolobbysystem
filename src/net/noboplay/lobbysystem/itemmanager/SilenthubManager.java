package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.core.api.BungeeMessaging;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class SilenthubManager implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR | event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Player player = event.getPlayer();
            try {
                if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Silent Hub")) {
                    BungeeMessaging.connectToServer(player, "SilentHub");

                } else if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Hub")) {
                    BungeeMessaging.connectToServer(player, "lobby");
                }
            } catch (Exception ex) {
            }
        }
    }
}
