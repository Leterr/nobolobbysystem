package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.Variables;
import net.noboplay.lobbysystem.util.Profil;
import net.noboplay.lobbysystem.util.Settings;
import net.noboplay.lobbysystem.util.Settings.SettingsOption;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ConfigManager implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        try {
            if (event.getInventory().getName().equalsIgnoreCase(ChatColor.GRAY + "" + ChatColor.UNDERLINE + "Einstellungen")) {
                if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_AQUA + "Doppelsprung")) {
                    if (!Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.DOUBLEJUMP)) {
                        Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.DOUBLEJUMP, true);
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast den " + ChatColor.DARK_AQUA + "Doppelsprung " + ChatColor.GREEN + "aktiviert!");
                        player.setAllowFlight(true);
                        player.setFlying(true);

                    } else {
                        Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.DOUBLEJUMP, false);
                        player.sendMessage(
                                LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast den " + ChatColor.DARK_AQUA + "Doppelsprung " + ChatColor.RED + "deaktiviert!");
                        player.setAllowFlight(false);
                        player.setFlying(false);
                    }

                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_AQUA + "Speed")) {
                    if (Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.SPEED)) {
                        Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.SPEED, false);
                        player.removePotionEffect(PotionEffectType.SPEED);
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast den " + ChatColor.DARK_AQUA + "Speed " + ChatColor.RED + "deaktiviert!");
                    } else {
                        Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.SPEED, true);
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast den " + ChatColor.DARK_AQUA + "Speed " + ChatColor.GREEN + "aktiviert!");
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 9999999, 3));
                    }
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "Zur" + Variables.CHAR_UE + "ck")) {
                    player.closeInventory();
                    Profil.openProfile(player);
                }
            }
        } catch (Exception exception) {
        }
    }
}
