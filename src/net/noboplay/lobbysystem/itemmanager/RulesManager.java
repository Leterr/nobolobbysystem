package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.util.RulesUtil;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class RulesManager implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getName().equalsIgnoreCase(ChatColor.RED + "Regeln")) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Ja")) {
                RulesUtil.acceptRules(player.getUniqueId().toString());
                player.closeInventory();
                player.sendMessage(LobbySystem.getInstance().PREFIX
                        + ChatColor.GREEN + "Vielen Dank! Wir w" + Variables.CHAR_UE + "nschen dir viel Spa" + Variables.CHAR_SZ + " auf " + ChatColor.AQUA + "NoboPlay.net.");
            } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Nein")) {
                player.kickPlayer(LobbySystem.getInstance().PREFIX
                        + ChatColor.RED + "Du wurdest gekickt, weil du unsere Regeln nicht akzeptierst.\nWeitere Infos auf " + ChatColor.AQUA + "www.NoboPlay.net");
            }

        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        Player player = (Player) event.getPlayer();
        if (event.getInventory().getName().equalsIgnoreCase(ChatColor.RED + "Regeln")) {
            if (!RulesUtil.isPlayerInSQL(player.getUniqueId().toString()))
                new BukkitRunnable() {

                    @Override
                    public void run() {
                        if (player != null) {
                            player.closeInventory();
                            RulesUtil.openRules(player);
                        }
                    }
                }.runTaskLaterAsynchronously(LobbySystem.getInstance(), 5);
        }
    }

    @EventHandler
    public void onOpen(InventoryOpenEvent event) {
        if (!RulesUtil.isPlayerInSQL(event.getPlayer().getUniqueId().toString())) {
            if (event.getInventory().getName().equalsIgnoreCase(ChatColor.RED + "Regeln"))
                return;
            else
                event.setCancelled(true);
        }
    }

}
