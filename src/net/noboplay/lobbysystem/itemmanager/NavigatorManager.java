package net.noboplay.lobbysystem.itemmanager;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.Variables;
import net.noboplay.lobbysystem.util.*;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class NavigatorManager implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        try {
            if (event.getItem() != null) {
                if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Navigator")
                        && RulesUtil.isPlayerInSQL(player.getUniqueId().toString())) {
                    NavigatorMenu.openNavigatorInv(player);
                }
            }
        } catch (Exception exception) {
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getInventory().getName().equalsIgnoreCase(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Was m" + Variables.CHAR_OE + "chtest du spielen?")) {
            try {
                event.setCancelled(true);
                if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Spawn")) {
                    SpawnLocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "SkyPVP")) {
                    SkyPVPLocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "SkyWars")) {
                    SWLocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Team")) {
                    TeamLocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "FFA")) {
                    FFALocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "NoboUnity")) {
                    NoboLocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "1vs1")) {
                    PVPLocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "SuperJump")) {
                    SJLocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(" ")) {
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Event")) {
                    EventLocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "QuickSG")) {
                    QSGLocation.teleportspawn(player);
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Premiumbereich")) {
                    if (player.hasPermission("server.premium")) {
                        PremiumLocation.teleportspawn(player);
                    } else {
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Hier darfst du nicht hin!");
                    }
                }
            } catch (Exception exception) {
                player.closeInventory();
            }
        }
    }
}
