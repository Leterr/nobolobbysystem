package net.noboplay.lobbysystem.util;

import net.noboplay.core.api.ActionBar;
import net.noboplay.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;

public class ActionBarCountdown {

    private static int i = 4;

    public static void startCountdown() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(LobbySystem.getInstance(), new Runnable() {

            @Override
            public void run() {
                if (i == 4) {
                    ActionBar.sendActionBar(LobbySystem.getInstance().getConfig().getString("Action5"));
                }
                if (i == 3) {
                    ActionBar.sendActionBar(LobbySystem.getInstance().getConfig().getString("Action4"));
                }
                if (i == 2) {
                    ActionBar.sendActionBar(LobbySystem.getInstance().getConfig().getString("Action3"));
                }
                if (i == 1) {
                    ActionBar.sendActionBar(LobbySystem.getInstance().getConfig().getString("Action2"));
                }
                if (i == 0) {
                    ActionBar.sendActionBar(LobbySystem.getInstance().getConfig().getString("Action1"));
                    i = 4;
                }
                i--;
            }
        }, 0, 20 * 20);
    }

}
