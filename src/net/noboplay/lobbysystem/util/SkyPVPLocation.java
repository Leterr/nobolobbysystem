package net.noboplay.lobbysystem.util;

import net.noboplay.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

public class SkyPVPLocation {

    public static void setspawnloc(Player player) {
        File file = new File("plugins/LobbySystem", "skypvp.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        Location location = player.getLocation();

        String world = player.getWorld().getName();
        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        double yaw = location.getYaw();
        double pitch = location.getPitch();

        cfg.set("spawns.spawn.world", world);
        cfg.set("spawns.spawn.x", x);
        cfg.set("spawns.spawn.y", y);
        cfg.set("spawns.spawn.z", z);
        cfg.set("spawns.spawn.yaw", yaw);
        cfg.set("spawns.spawn.pitch", pitch);
        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Du hast erfolgreich " + ChatColor.GOLD + "SkyPVP" + ChatColor.DARK_AQUA + " gesetzt!");
        player.playSound(player.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);

    }

    public static void teleportspawn(Player player) {
        File file = new File("plugins/LobbySystem", "skypvp.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        String world = cfg.getString("spawns.spawn.world");
        double x = cfg.getDouble("spawns.spawn.x");
        double y = cfg.getDouble("spawns.spawn.y");
        double z = cfg.getDouble("spawns.spawn.z");
        double yaw = cfg.getDouble("spawns.spawn.yaw");
        double pitch = cfg.getDouble("spawns.spawn.pitch");

        Location location = new Location(Bukkit.getWorld(world), x, y, z);
        location.setYaw((float) yaw);
        location.setPitch((float) pitch);

        player.teleport(location);

    }

}
