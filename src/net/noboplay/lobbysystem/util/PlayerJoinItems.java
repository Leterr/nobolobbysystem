package net.noboplay.lobbysystem.util;

import net.noboplay.core.api.Core;
import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class PlayerJoinItems {

    public static void joinItems(final Player player) {
        final ItemStack[] silenthub = new ItemStack[1];

        ItemStack navigator = ItemStackManager.createItem(ChatColor.YELLOW + "Navigator", ChatColor.GRAY + "Teleportiere dich zu den Minigames",
                Material.COMPASS, 1, 0);
        ItemStack profil = ItemStackManager.createHead(ChatColor.GRAY + "Dein Profil", ChatColor.GRAY + "Shop, Einstellungen und vieles mehr",
                player.getName());
        final ItemStack schutzschild = ItemStackManager.createItem(ChatColor.DARK_PURPLE + "Schutzschild", ChatColor.GRAY + "Schleudert Leute weg",
                Material.EYE_OF_ENDER, 1, 0);
        Bukkit.getScheduler().runTaskLaterAsynchronously(LobbySystem.getInstance(), () -> {
            if (Core.getServerName().toLowerCase().startsWith("silenthub")) {
                silenthub[0] = ItemStackManager.createItem(ChatColor.GREEN + "Hub", ChatColor.GRAY + "Gehe wieder zu den anderen",
                        Material.TNT, 1, 0);
            } else {
                silenthub[0] = ItemStackManager.createItem(ChatColor.DARK_RED + "Silent Hub", ChatColor.GRAY + "Ungest" + Variables.CHAR_OE + "rt und alleine sein",
                        Material.TNT, 1, 0);
            }
            if (player.hasPermission("server.youtuber") || player.hasPermission("server.team")) {
                player.getInventory().setItem(2, schutzschild);
                player.getInventory().setItem(6, silenthub[0]);
            }
        }, 20);

        ItemStack pearl = ItemStackManager.createItem(ChatColor.DARK_PURPLE + "EnderBow", null, Material.BOW, 1, 0);
        ItemStackManager.setUnbreakable(pearl, true);
        ItemStackManager.enchantItem(pearl, Enchantment.DURABILITY, 1);
        ItemStackManager.addItemFlag(pearl, ItemFlag.HIDE_ENCHANTS);

        player.getInventory().setItem(0, navigator);
        player.getInventory().setItem(1, profil);
        player.getInventory().setItem(8, pearl);

    }

}
