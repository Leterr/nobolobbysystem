package net.noboplay.lobbysystem.util;

import net.noboplay.lobbysystem.Variables;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class Gadgets {

    public static void openGadgets(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 18, ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Gadgets");

        ItemStack clothing = ItemStackManager.createItem(ChatColor.GRAY + "Kleiderschrank", ChatColor.GRAY + "Neuer Style gef" + Variables.CHAR_AE + "lig?",
                Material.LEATHER_CHESTPLATE, 1, 0);
        ItemStack trolltnt = ItemStackManager.createItem(ChatColor.DARK_RED + "Troll-TNT", ChatColor.GRAY + "Trollen? Ja bitte!", Material.TNT, 1,
                0);
        ItemStack flystick = ItemStackManager.createItem(ChatColor.GOLD + "Flugstab", ChatColor.GRAY + "Flieg, flieg", Material.BLAZE_POWDER, 1,
                0);
        ItemStack pearl = ItemStackManager.createItem(ChatColor.DARK_PURPLE + "EnderBow", null, Material.BOW, 1, 0);
        ItemStackManager.enchantItem(pearl, Enchantment.DURABILITY, 1);
        ItemStackManager.addItemFlag(pearl, ItemFlag.HIDE_ENCHANTS);
        ItemStack back = ItemStackManager.createItem(ChatColor.GRAY + "Zur" + Variables.CHAR_UE + "ck", null, Material.BARRIER, 1, 0);

        inventory.setItem(0, clothing);
        inventory.setItem(4, trolltnt);
        inventory.setItem(8, flystick);
        inventory.setItem(9, pearl);
        inventory.setItem(17, back);

        player.openInventory(inventory);
    }
}
