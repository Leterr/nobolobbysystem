package net.noboplay.lobbysystem.util;

import net.noboplay.lobbysystem.Variables;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class NavigatorMenu {

    public static void openNavigatorInv(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 27, ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Was m" + Variables.CHAR_OE + "chtest du spielen?");

        ItemStack spawn = ItemStackManager.createItem(ChatColor.AQUA + "Spawn", ChatColor.GRAY + "Zum Spawn", Material.MAGMA_CREAM, 1, 0);
        ItemStack skypvp = ItemStackManager.createItem(ChatColor.AQUA + "SkyPVP", ChatColor.GRAY + "Zu SkyPVP", Material.BOW, 1, 0);
        ItemStack ffa = ItemStackManager.createItem(ChatColor.AQUA + "FFA", ChatColor.GRAY + "Zu FreeForAll", Material.IRON_CHESTPLATE, 1, 0);
        ItemStack sw = ItemStackManager.createItem(ChatColor.AQUA + "SkyWars", ChatColor.GRAY + "Zu SkyWars", Material.GRASS, 1, 0);
        ItemStack sj = ItemStackManager.createItem(ChatColor.AQUA + "SuperJump", ChatColor.GRAY + "Zu SuperJump", Material.LEATHER_BOOTS, 1, 0);
        ItemStack pvp = ItemStackManager.createItem(ChatColor.AQUA + "1vs1", ChatColor.GRAY + "Zu 1vs1", Material.FISHING_ROD, 1, 0);
        ItemStack nobo = ItemStackManager.createItem(ChatColor.AQUA + "NoboUnity", ChatColor.GRAY + "Zur NoboUnity", Material.COOKIE, 1, 0);
        ItemStack premium = ItemStackManager.createItem(ChatColor.GOLD + "Premiumbereich", ChatColor.GRAY + "In den wunderbaren Welten sein",
                Material.SPECKLED_MELON, 1, 0);
        ItemStack team = ItemStackManager.createHead(ChatColor.AQUA + "Team", ChatColor.GRAY + "Das Team", player.getName());
        ItemStack event = ItemStackManager.createItem(ChatColor.AQUA + "Event", ChatColor.GRAY + "Zu den Events", Material.GOLD_NUGGET, 1, 0);
        ItemStack qsg = ItemStackManager.createItem(ChatColor.AQUA + "QuickSG", ChatColor.GRAY + "Zu QuickSG", Material.IRON_SWORD, 1, 0);

        ItemStack red = ItemStackManager.createItem(" ", null, Material.STAINED_GLASS_PANE, 1, 14);
        ItemStack yellow = ItemStackManager.createItem(" ", null, Material.STAINED_GLASS_PANE, 1, 4);
        ItemStack black = ItemStackManager.createItem(" ", null, Material.STAINED_GLASS_PANE, 1, 15);

        inventory.setItem(0, sw);
        inventory.setItem(1, red);
        inventory.setItem(2, yellow);
        inventory.setItem(3, nobo);
        inventory.setItem(4, black);
        inventory.setItem(5, pvp);
        inventory.setItem(6, yellow);
        inventory.setItem(7, red);
        inventory.setItem(8, premium);
        inventory.setItem(9, red);
        inventory.setItem(10, event);
        inventory.setItem(11, yellow);
        inventory.setItem(12, black);
        inventory.setItem(13, spawn);
        inventory.setItem(14, black);
        inventory.setItem(15, yellow);
        inventory.setItem(16, qsg);
        inventory.setItem(17, red);
        inventory.setItem(18, ffa);
        inventory.setItem(19, red);
        inventory.setItem(20, yellow);
        inventory.setItem(21, skypvp);
        inventory.setItem(22, black);
        inventory.setItem(23, sj);
        inventory.setItem(24, yellow);
        inventory.setItem(25, red);
        inventory.setItem(26, team);

        player.openInventory(inventory);
    }

}
