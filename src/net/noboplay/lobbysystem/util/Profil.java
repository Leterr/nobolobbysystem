package net.noboplay.lobbysystem.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public class Profil {

    public static void openProfile(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 27, ChatColor.GREEN + "Dein Profil");

        ItemStack hide = ItemStackManager.createItem(ChatColor.YELLOW + "Spieler verstecken",
                ChatColor.GRAY + "Verstecke Spieler die du nicht sehen willst", Material.POTION, 1, 8238);
        ItemStack config = ItemStackManager.createItem(ChatColor.DARK_AQUA + "Einstellungen", ChatColor.GRAY + "Hier kannst du alle Einstellungen vornehmen",
                Material.REDSTONE, 1, 0);
        ItemStack gadgets = ItemStackManager.createItem(ChatColor.GREEN + "Gadgets", ChatColor.GRAY + "Have Fun! :)", Material.DOUBLE_PLANT, 1,
                0);
        ItemStack shop = ItemStackManager.createItem(ChatColor.YELLOW + "Shop", ChatColor.GRAY + "Kaufe dir Gadgets", Material.GOLD_NUGGET, 1, 0);
        ItemStack friends = ItemStackManager.createHead(ChatColor.DARK_PURPLE + "Freunde", ChatColor.GRAY + "Zeige deine Freunde an", player.getName());

        ItemStack blue = ItemStackManager.createItem(" ", null, Material.STAINED_GLASS_PANE, 1, 11);
        ItemStack yellow = ItemStackManager.createItem(" ", null, Material.STAINED_GLASS_PANE, 1, 4);
        ItemStack black = ItemStackManager.createItem(" ", null, Material.STAINED_GLASS_PANE, 1, 15);
        ItemStackManager.addItemFlag(hide, ItemFlag.HIDE_POTION_EFFECTS);

        inventory.setItem(0, black);
        inventory.setItem(1, black);
        inventory.setItem(2, yellow);
        inventory.setItem(3, black);
        inventory.setItem(4, config);
        inventory.setItem(5, black);
        inventory.setItem(6, yellow);
        inventory.setItem(7, black);
        inventory.setItem(8, black);
        inventory.setItem(9, yellow);
        inventory.setItem(10, blue);
        inventory.setItem(11, shop);
        inventory.setItem(12, yellow);
        inventory.setItem(13, friends);
        inventory.setItem(14, yellow);
        inventory.setItem(15, gadgets);
        inventory.setItem(16, blue);
        inventory.setItem(17, yellow);
        inventory.setItem(18, black);
        inventory.setItem(19, black);
        inventory.setItem(20, yellow);
        inventory.setItem(21, black);
        inventory.setItem(22, hide);
        inventory.setItem(23, black);
        inventory.setItem(24, yellow);
        inventory.setItem(25, black);
        inventory.setItem(26, black);

        player.openInventory(inventory);
    }

}
