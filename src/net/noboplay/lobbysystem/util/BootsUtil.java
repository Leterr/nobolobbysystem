package net.noboplay.lobbysystem.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BootsUtil {

	public static boolean playerExists(String uuid) {
		try {
			PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM Boots WHERE UUID=?");
			statement.setString(1, uuid);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				return rs.getString("UUID") != null;
			}
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void createPlayer(String uuid) {
		if (!playerExists(uuid)) {
			try {
				PreparedStatement statement = MySQL.getConnection().prepareStatement(
						"INSERT INTO Boots(UUID, boot1, boot2, boot3, boot4, boot5, boot6) VALUES (?, false, false, false, false, false, false);");
				statement.setString(1, uuid);
				statement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean getBoots(String uuid, int boots) {
		if (playerExists(uuid)) {
			try {
				PreparedStatement statement = MySQL.getConnection()
						.prepareStatement("SELECT boot? FROM Boots WHERE UUID=?");
				statement.setInt(1, boots);
				statement.setString(2, uuid);
				ResultSet rs = statement.executeQuery();
				if (rs.next()) {
					return rs.getBoolean("boot" + boots);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			createPlayer(uuid);
			getBoots(uuid, boots);
		}
		return false;
	}

	public static void giveBoots(String uuid, int boots) {
		if (playerExists(uuid)) {
			try {
				PreparedStatement statement = MySQL.getConnection()
						.prepareStatement("UPDATE Boots SET boot?=1 WHERE UUID=?;");
				statement.setInt(1, boots);
				statement.setString(2, uuid);
				statement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			createPlayer(uuid);
			giveBoots(uuid, boots);
		}
	}

}
