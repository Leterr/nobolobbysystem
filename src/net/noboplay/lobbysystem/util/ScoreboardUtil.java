package net.noboplay.lobbysystem.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import net.noboplay.core.api.Tokens;

public class ScoreboardUtil implements Listener {

	@SuppressWarnings("deprecation")
	public static void updateNametag(Player player) {
		for (Player all : Bukkit.getOnlinePlayers()) {
			Scoreboard board = player.getScoreboard();
			if (board == null) {
				board = Bukkit.getScoreboardManager().getNewScoreboard();
				player.setScoreboard(board);
			}
			if (all.hasPermission("server.admin")) {
				if (board.getTeam("0001admin") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0001admin").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0001admin");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0001admin").setPrefix("§4Admin §8▏ §7");
					board.getTeam("0001admin").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			} else if (all.hasPermission("server.developer")) {
				if (board.getTeam("0002developer") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0002developer").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0002developer");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0002developer").setPrefix("§bDev §8▏ §7");
					board.getTeam("0002developer").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			} else if (all.hasPermission("server.srmoderator")) {
				if (board.getTeam("0003srmod") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0003srmod").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0003srmod");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0003srmod").setPrefix("§cSrMod §8▏ §7");
					board.getTeam("0003srmod").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			} else if (all.hasPermission("server.moderator")) {
				if (board.getTeam("0004mod") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0004mod").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0004mod");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0004mod").setPrefix("§cMod §8▏ §7");
					board.getTeam("0004mod").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			} else if (all.hasPermission("server.supporter")) {
				if (board.getTeam("0005Supp") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0005Supp").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0005Supp");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0005Supp").setPrefix("§3Supp §8▏ §7");
					board.getTeam("0005Supp").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			} else if (all.hasPermission("server.builder")) {
				if (board.getTeam("0006Builder") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0006Builder").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0006Builder");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0006Builder").setPrefix("§eBuilder §8▏ §7");
					board.getTeam("0006Builder").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			} else if (all.hasPermission("server.youtuber")) {
				if (board.getTeam("0007YT") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0007YT").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0007YT");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0007YT").setPrefix("§5");
					board.getTeam("0007YT").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			} else if (all.hasPermission("server.premium+")) {
				if (board.getTeam("0008YTJR") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0008YTJR").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0008YTJR");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0008YTJR").setPrefix("§6");
					board.getTeam("0008YTJR").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			} else if (all.hasPermission("server.premium")) {
				if (board.getTeam("0009PREM") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0009PREM").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0009PREM");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0009PREM").setPrefix("§6");
					board.getTeam("0009PREM").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			} else {
				if (board.getTeam("0010User") != null) {
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0010User").addPlayer(all);
					player.setScoreboard(board);
				} else {
					board.registerNewTeam("0010User");
					for (Team team : board.getTeams()) {
						team.removePlayer(all);
					}
					board.getTeam("0010User").setPrefix("§a");
					board.getTeam("0010User").addPlayer(all);
					player.setScoreboard(board);
				}
				continue;
			}
		}
	}

	public static void setScoreboard(Player player) {
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		String rang = RangUtil.getRang(player);
		if (board.getObjective("info") == null) {
			Objective obj = board.registerNewObjective("info", "aaa");
			obj.setDisplaySlot(DisplaySlot.SIDEBAR);
			obj.setDisplayName("§bNoboPlay.net");
			Score ten = obj.getScore("§3Dein Rang:");
			Score nine = obj.getScore("§c" + rang);
			Score eigth = obj.getScore("§a ");
			Score seven = obj.getScore("§3Tokens:");
			Score six = obj.getScore("§b" + Tokens.getTokens(player.getUniqueId().toString()));
			Score five = obj.getScore("§e ");
			Score four = obj.getScore("§3Forum:");
			Score three = obj.getScore("§bNoboPlay.net §r");
			Score two = obj.getScore("§r ");
			Score one = obj.getScore("§3TeamSpeak:");
			Score zero = obj.getScore("§bNoboPlay.net");

			ten.setScore(10);
			nine.setScore(9);
			eigth.setScore(8);
			seven.setScore(7);
			six.setScore(6);
			five.setScore(5);
			four.setScore(4);
			three.setScore(3);
			two.setScore(2);
			one.setScore(1);
			zero.setScore(0);
		}
		player.setScoreboard(board);
	}
}