package net.noboplay.lobbysystem.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by steven on 26.03.16.
 */
public class Settings {

    public static boolean isPlayerInSQL(String uuid) {
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM Settings WHERE UUID=?");
            statement.setString(1, uuid);
            ResultSet set = statement.executeQuery();
            if (set.next()) return set.getString("UUID") != null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void createPlayer(String uuid) {
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("INSERT INTO Settings(UUID, doublejump, fly, hidden, speed, flystick, armor) VALUES (?, false, false, false, false, false, null);");
            statement.setString(1, uuid);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setBoolean(String uuid, SettingsOption option, boolean value) {
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("UPDATE Settings SET " + option.toString() + "=? WHERE UUID=?");
            statement.setString(1, (value ? "1" : "0"));
            statement.setString(2, uuid);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean getBoolean(String uuid, SettingsOption option) {
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM Settings WHERE UUID=?");
            statement.setString(1, uuid);
            ResultSet set = statement.executeQuery();
            if (set.next()) return set.getBoolean(option.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setArmor(String uuid, String armor) {
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("UPDATE Settings SET armor=? WHERE UUID=?");
            statement.setString(1, armor);
            statement.setString(2, uuid);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getArmor(String uuid) {
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM Settings WHERE UUID=?");
            statement.setString(1, uuid);
            ResultSet set = statement.executeQuery();
            if (set.next()) return set.getString("armor");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public enum SettingsOption {

        DOUBLEJUMP("doublejump"), FLY("fly"), HIDDEN("hidden"), SPEED("speed"), FLYSTICK("flystick");

        SettingsOption(String name) {
            this.name = name;
        }

        private String name;

        @Override
        public String toString() {
            return name;
        }
    }

}
