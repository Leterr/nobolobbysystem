package net.noboplay.lobbysystem.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RulesUtil {

    public static void openRules(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.RED + "Regeln");

        ItemStack yes = ItemStackManager.createItem(ChatColor.GREEN + "Ja", ChatColor.GRAY + "Du akzeptierst die Regeln", Material.WOOL, 1, 5);
        ItemStack infos = ItemStackManager.createItem(ChatColor.AQUA + "Regeln",
                ChatColor.GRAY + "Die Regeln findest du unter " + ChatColor.AQUA + "www.NoboPlay.net", Material.PAPER, 0, 0);
        ItemStack no = ItemStackManager.createItem(ChatColor.DARK_RED + "Nein", ChatColor.GRAY + "Du akzeptierst nicht die Regeln", Material.WOOL, 1,
                14);

        inventory.setItem(0, yes);
        inventory.setItem(4, infos);
        inventory.setItem(8, no);

        player.openInventory(inventory);
    }

    public static boolean isPlayerInSQL(String uuid) {
        try {
            PreparedStatement statement = MySQL.getConnection().prepareStatement("SELECT * FROM Rules WHERE UUID=?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return rs.getString("UUID") != null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void acceptRules(String uuid) {
        try {
            PreparedStatement statement = MySQL.getConnection()
                    .prepareStatement("INSERT INTO Rules(UUID, accepted) VALUES (?, true);");
            statement.setString(1, uuid);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
