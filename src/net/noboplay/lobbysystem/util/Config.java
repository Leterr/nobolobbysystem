package net.noboplay.lobbysystem.util;

import net.noboplay.lobbysystem.Variables;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Config {

    public static void openConfig(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.AQUA + "" + ChatColor.UNDERLINE + "Einstellungen");

        ItemStack doublejump = ItemStackManager.createItem(ChatColor.DARK_AQUA + "Doppelsprung", ChatColor.GRAY + "Springe hoch hinaus",
                Material.DIAMOND_BOOTS, 1, 0);
        ItemStack speed = ItemStackManager.createItem(ChatColor.DARK_AQUA + "Speed", ChatColor.GRAY + "Sei der Porsche auf zwei Beinen",
                Material.SUGAR, 1, 0);
        ItemStack back = ItemStackManager.createItem(ChatColor.GRAY + "Zur" + Variables.CHAR_UE + "ck", null, Material.BARRIER, 1, 0);

        inventory.setItem(0, doublejump);
        inventory.setItem(4, speed);
        inventory.setItem(8, back);

        player.openInventory(inventory);
    }

}
