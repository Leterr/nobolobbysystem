package net.noboplay.lobbysystem.util;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.server.v1_8_R3.PacketDataSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutCustomPayload;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by steven on 26.03.16.
 */
public class LabyModFeatures {

    private static Map<EnumLabyModFeature, Boolean> map = new HashMap<>();

    public static void setLabyModFeature(Player player, Map<EnumLabyModFeature, Boolean> map) {
        if (player.hasPermission("server.team")) return;
        try {
            Map<String, Boolean> list = new HashMap<>();
            map.keySet().forEach(feature -> list.put(feature.name(), map.get(feature)));
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            ObjectOutputStream objectStream = new ObjectOutputStream(byteStream);
            objectStream.writeObject(list);
            ByteBuf buf = Unpooled.copiedBuffer(byteStream.toByteArray());
            PacketDataSerializer serializer = new PacketDataSerializer(buf);
            PacketPlayOutCustomPayload payload = new PacketPlayOutCustomPayload("LABYMOD", serializer);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(payload);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void initLabyMod() {
        map.put(EnumLabyModFeature.DAMAGEINDICATOR, false);
        map.put(EnumLabyModFeature.NICK, false);
        map.put(EnumLabyModFeature.MINIMAP_RADAR, false);
    }

    public static Map<EnumLabyModFeature, Boolean> getFeatureList() {
        return map;
    }

    public enum EnumLabyModFeature {
        FOOD, GUI, NICK, BLOCKBUILD, CHAT, EXTRAS, ANIMATIONS, POTIONS, ARMOR, DAMAGEINDICATOR, MINIMAP_RADAR;
    }

}
