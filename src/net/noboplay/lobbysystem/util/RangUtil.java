package net.noboplay.lobbysystem.util;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class RangUtil {

    public static String getRang(Player player) {
        String rang;
        if (player.hasPermission("server.admin")) {
            rang = ChatColor.DARK_RED + "Administrator";
            return rang;
        } else if (player.hasPermission("server.developer")) {
            rang = ChatColor.AQUA + "Developer";
            return rang;
        } else if (player.hasPermission("server.builder")) {
            rang = ChatColor.YELLOW + "Builder";
            return rang;
        } else if (player.hasPermission("server.srmoderator")) {
            rang = ChatColor.RED + "SrModerator";
            return rang;
        } else if (player.hasPermission("server.moderator")) {
            rang = ChatColor.RED + "Moderator";
            return rang;
        } else if (player.hasPermission("server.supporter")) {
            rang = ChatColor.DARK_AQUA + "Supporter";
            return rang;
        } else if (player.hasPermission("server.premium+")) {
            rang = ChatColor.GOLD + "YouTuberJr";
            return rang;
        } else if (player.hasPermission("server.youtuber")) {
            rang = ChatColor.DARK_PURPLE + "YouTuber";
            return rang;
        } else if (player.hasPermission("server.premium")) {
            rang = ChatColor.GOLD + "Premium";
            return rang;
        } else {
            rang = ChatColor.GREEN + "Member";
        }
        return rang;
    }

}
