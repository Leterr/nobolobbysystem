package net.noboplay.lobbysystem.util;

import net.noboplay.lobbysystem.Variables;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL {

    static Connection con;

    public static void connect() {
        File file = new File("plugins/LobbySystem", "mysql.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        try {
            con = DriverManager.getConnection(
                    "jdbc:mysql://" + cfg.getString("Host") + "/" + cfg.getString("Database") + "?autoReconnect=true",
                    cfg.getString("User"), cfg.getString("Password"));
            System.out.println("NoboPlay " + Variables.CHAR_DOUBLE_ARROWS + " MySQL-Verbindung wurde aufgebaut!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void close() {
        try {
            if (con != null) {
                con.close();
                System.out.println("NoboPlay " + Variables.CHAR_DOUBLE_ARROWS + " MySQL-Verbindung wurde geschlossen!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return con;
    }

    public static void createFile() {
        File file = new File("plugins/lobbysystem", "mysql.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        cfg.addDefault("Host", "127.0.0.1");
        cfg.addDefault("Database", "Token");
        cfg.addDefault("User", "root");
        cfg.addDefault("Password", "password");
        cfg.options().copyDefaults(true);
        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
