package net.noboplay.lobbysystem.util;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Shop {

    public static void openShop(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 18, ChatColor.GREEN + "Shop");

        ItemStack heads = ItemStackManager.createHead(ChatColor.AQUA + "K" + Variables.CHAR_OE + "pfe", ChatColor.GRAY + "Neuer Kopf gef" + Variables.CHAR_AE + "llig?", "MHF_Question");
        ItemStack boots = ItemStackManager.createItem(ChatColor.AQUA + "Boots", ChatColor.GRAY + "Mit Keks-Boots :3", Material.LEATHER_BOOTS, 1,
                0);
        ItemStack pets = ItemStackManager.createItem(ChatColor.AQUA + "Pets", ChatColor.GRAY + "Werde zum Herrchen", Material.MONSTER_EGG, 1, 0);
        ItemStack back = ItemStackManager.createItem(ChatColor.AQUA + "Zur" + Variables.CHAR_UE + "ck", "", Material.BARRIER, 1, 0);

        inventory.setItem(0, heads);
        inventory.setItem(4, boots);
        inventory.setItem(8, pets);
        inventory.setItem(17, back);

        player.openInventory(inventory);
    }

    public static void openHeads(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.AQUA + "K" + Variables.CHAR_OE + "pfe");

        ItemStack max = ItemStackManager.createHead(ChatColor.DARK_RED + "Noboplays", "", "Noboplays");
        ItemStack steven = ItemStackManager.createHead(ChatColor.DARK_RED + "Zocker_SK", "", "Zocker_SK");
        ItemStack kai = ItemStackManager.createHead(ChatColor.DARK_RED + "Leterr", "", "Leterr");
        ItemStack michi = ItemStackManager.createHead(ChatColor.DARK_RED + "MichiVIP", "", "MichiVIP");
        ItemStack noel = ItemStackManager.createHead(ChatColor.DARK_RED + "DevCoffe", "", "DevCoffe");
        ItemStack remove = ItemStackManager.createHead("�cKopf entfernen", "", "MHF_Question");
        ItemStack back = ItemStackManager.createItem("�7Zur�ck", "", Material.BARRIER, 1, 0);

        inventory.setItem(0, max);
        inventory.setItem(1, steven);
        inventory.setItem(2, kai);
        inventory.setItem(3, michi);
        inventory.setItem(4, noel);
        inventory.setItem(7, remove);
        inventory.setItem(8, back);

        player.openInventory(inventory);
    }

    public static void openPets(Player player) {
        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Folgt demn" + Variables.CHAR_AE + "chst...");
    }

}
