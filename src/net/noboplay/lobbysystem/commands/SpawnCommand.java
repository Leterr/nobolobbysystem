package net.noboplay.lobbysystem.commands;

import net.noboplay.lobbysystem.util.SpawnLocation;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String commandLabel, String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            SpawnLocation.teleportspawn(player);
        }
        return true;
    }

}
