package net.noboplay.lobbysystem.commands;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.util.ItemStackManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class HeadCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String commandLabel, String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (player.hasPermission("server.builder")) {
                if (args.length == 0) {
                    player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Syntax: /head <NAME>");
                } else if (args.length == 1) {
                    ItemStack head = ItemStackManager.createHead(args[0], null, args[0]);
                    player.getInventory().addItem(head);
                }
            } else {
                player.sendMessage(LobbySystem.getInstance().PREFIX + LobbySystem.getInstance().NOPERM);
            }
        }
        return true;
    }
}
