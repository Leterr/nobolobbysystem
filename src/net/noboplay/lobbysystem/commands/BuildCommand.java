package net.noboplay.lobbysystem.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.noboplay.lobbysystem.LobbySystem;

public class BuildCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String commandLabel, String[] args) {
		if (commandSender instanceof Player) {
			Player player = (Player) commandSender;
			if (player.hasPermission("server.build")) {
				if (!LobbySystem.getInstance().build.contains(player.getName())) {
					LobbySystem.getInstance().build.add(player.getName());
					player.sendMessage(LobbySystem.getInstance().PREFIX + "�3Du bist nun im �eBuildmodus�3!");
				} else {
					LobbySystem.getInstance().build.remove(player.getName());
					player.sendMessage(
							LobbySystem.getInstance().PREFIX + "�3Du bist nun nicht mehr im �eBuildmodus�3!");
				}
			}
		}
		return true;
	}

}
