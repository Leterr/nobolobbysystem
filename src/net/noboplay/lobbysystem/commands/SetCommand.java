package net.noboplay.lobbysystem.commands;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.util.*;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String commandLabel, String[] args) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (player.hasPermission("server.admin")) {
                if (args.length == 0) {
                    player.sendMessage(ChatColor.AQUA + "/set spawn: Setze den Spawn");
                    player.sendMessage(ChatColor.AQUA + "/set premium: Setze Premiumbereich");
                    player.sendMessage(ChatColor.AQUA + "/set team: Setze Teambereich");
                    player.sendMessage(ChatColor.AQUA + "/set nobo: Setze NoboUnity");
                    player.sendMessage(ChatColor.AQUA + "/set ffa: Setze FFA");
                    player.sendMessage(ChatColor.AQUA + "/set skypvp: Setze SkyPVP");
                    player.sendMessage(ChatColor.AQUA + "/set sw: Setze SkyWars");
                    player.sendMessage(ChatColor.AQUA + "/set sj: Setze SuperJump");
                    player.sendMessage(ChatColor.AQUA + "/set pvp: Setze 1vs1");
                    player.sendMessage(ChatColor.AQUA + "/set event: Setze Event");
                    player.sendMessage(ChatColor.AQUA + "/set qsg: Setze QuickSG");
                } else if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("spawn")) {
                        SpawnLocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("premium")) {
                        PremiumLocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("team")) {
                        TeamLocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("nobo")) {
                        NoboLocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("ffa")) {
                        FFALocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("skypvp")) {
                        SkyPVPLocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("sw")) {
                        SWLocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("sj")) {
                        SJLocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("pvp")) {
                        PVPLocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("event")) {
                        EventLocation.setspawnloc(player);
                        return true;
                    } else if (args[0].equalsIgnoreCase("qsg")) {
                        QSGLocation.setspawnloc(player);
                        return true;
                    }
                }
            } else {
                player.sendMessage(LobbySystem.getInstance().PREFIX + LobbySystem.getInstance().NOPERM);
            }
        }
        return true;
    }
}
