package net.noboplay.lobbysystem.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.noboplay.lobbysystem.boots.Boots;

public class BootsCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String commandLabel, String[] args) {
		if (commandSender instanceof Player) {
			Player player = (Player) commandSender;
			Boots.openShop(player);
		}
		return true;
	}

}
