package net.noboplay.lobbysystem.commands;

import net.noboplay.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("server.team")) {
                if (args.length == 1) {
                    Player target = Bukkit.getPlayer(args[0].trim());
                    if (target == null)
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Der Spieler ist nicht online.");
                    else {
                        player.teleport(target);
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast dich zu "
                                + target.getDisplayName() + ChatColor.GREEN + " teleportiert.");
                    }
                } else if (args.length == 2) {
                    if (player.hasPermission("server.admin")) {
                        Player target = Bukkit.getPlayer(args[0].trim());
                        Player targetto = Bukkit.getPlayer(args[1].trim());
                        if (target == null) {
                            player.sendMessage(
                                    LobbySystem.getInstance().PREFIX + ChatColor.GOLD + args[0].trim() + ChatColor.RED + " ist nicht online.");
                            return true;
                        }
                        if (targetto == null) {
                            player.sendMessage(
                                    LobbySystem.getInstance().PREFIX + ChatColor.GOLD + args[1].trim() + ChatColor.RED + " ist nicht online.");
                            return true;
                        }
                        target.teleport(targetto);
                        player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast " + target.getDisplayName() +
                                ChatColor.GREEN + " zu " + targetto.getDisplayName() + ChatColor.GREEN + " teleportiert.");
                    } else
                        player.sendMessage(LobbySystem.getInstance().PREFIX + LobbySystem.getInstance().NOPERM);
                } else
                    player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Syntax: /tp <NAME> (<NAME>)");
            } else
                player.sendMessage(LobbySystem.getInstance().PREFIX + LobbySystem.getInstance().NOPERM);
        }
        return true;
    }

}
