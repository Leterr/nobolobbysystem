package net.noboplay.lobbysystem.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class PremCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
		if (args.length == 2) {
			StringBuffer buffer = new StringBuffer(args[1].trim());
			buffer.insert(8, "-").insert(13, "-").insert(18, "-").insert(23, "-");
			if (args[0].equalsIgnoreCase("add")) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
						"pex user " + buffer.toString() + " group set premium");
			} else if (args[0].equalsIgnoreCase("remove")) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
						"pex user " + buffer.toString() + " group set member");
			}
		}
		return true;
	}

}
