package net.noboplay.lobbysystem.commands;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.util.Settings;
import net.noboplay.lobbysystem.util.Settings.SettingsOption;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String commandLabel, String[] args) {
        Player player = (Player) commandSender;
        if (player.hasPermission("server.team")) {
            if (args.length == 0) {
                if (Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.FLY)) {
                    Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.FLY, false);
                    player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Fly wurde " + ChatColor.RED + "deaktiviert");
                    player.setAllowFlight(false);
                } else {
                    Settings.setBoolean(player.getUniqueId().toString(), SettingsOption.FLY, true);
                    player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Fly wurde " + ChatColor.GREEN + "aktivert");
                    player.setAllowFlight(true);
                }
            } else if (args.length == 1) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target == null) {
                    player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Der Spieler ist nicht online!");
                    return true;
                }
                if (Settings.getBoolean(target.getUniqueId().toString(), SettingsOption.FLY)) {
                    Settings.setBoolean(target.getUniqueId().toString(), SettingsOption.FLY, false);
                    player.sendMessage(
                            LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Fly von " + ChatColor.AQUA + args[0] + ChatColor.DARK_AQUA + " wurde " + ChatColor.RED + "deaktiviert");
                    target.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Fly wurde " + ChatColor.RED + "deaktiviert");
                    target.setAllowFlight(false);
                } else {
                    Settings.setBoolean(target.getUniqueId().toString(), SettingsOption.FLY, true);
                    player.sendMessage(
                            LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Fly von " + ChatColor.AQUA + args[0] + ChatColor.DARK_AQUA + " wurde " + ChatColor.GREEN + "aktiviert");
                    target.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.DARK_AQUA + "Fly wurde " + ChatColor.GREEN + "aktiviert");
                    target.setAllowFlight(true);
                }
            } else {
                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Syntax: /fly <NAME>");
            }
        } else {
            player.sendMessage(LobbySystem.getInstance().PREFIX + LobbySystem.getInstance().NOPERM);
        }
        return true;
    }

}
