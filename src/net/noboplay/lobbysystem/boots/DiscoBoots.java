package net.noboplay.lobbysystem.boots;

import net.noboplay.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.Random;

public class DiscoBoots {

    private static void changeBoots(Player player) {
        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
        LeatherArmorMeta meta = (LeatherArmorMeta) boots.getItemMeta();
        meta.setColor(Color.fromRGB(new Random().nextInt(255) + 1, new Random().nextInt(255) + 1,
                new Random().nextInt(255) + 1));
        meta.setDisplayName(ChatColor.GOLD + "Disco-Boots");
        boots.setItemMeta(meta);
        player.getInventory().setBoots(boots);
    }

    public static void startThread() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(LobbySystem.getInstance(), () -> LobbySystem.getInstance().discoplayer.forEach(DiscoBoots::changeBoots), 0, 5);
    }

}
