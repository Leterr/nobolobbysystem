package net.noboplay.lobbysystem.boots;

import net.noboplay.lobbysystem.util.BootsUtil;
import net.noboplay.lobbysystem.util.ItemStackManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Boots {

    public static void openShop(Player player) {
        Inventory inventory = Bukkit.createInventory(null, 18, ChatColor.AQUA + "Boots");
        ItemStack love = ItemStackManager.createBoots(ChatColor.RED + "Love-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 1) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.PURPLE);
        ItemStack fire = ItemStackManager.createBoots(ChatColor.DARK_RED + "Fire-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 2) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.RED);
        ItemStack smoke = ItemStackManager.createBoots(ChatColor.DARK_AQUA + "Smoke-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 3) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.GRAY);
        ItemStack angry = ItemStackManager.createBoots(ChatColor.DARK_RED + "Angry-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 4) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.RED);
        ItemStack cloud = ItemStackManager.createBoots(ChatColor.AQUA + "Cloud-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 5) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.WHITE);
        ItemStack cookie = ItemStackManager.createBoots(ChatColor.GOLD + "Keks-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 6) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.FUCHSIA);
        ItemStack disco = ItemStackManager.createBoots(ChatColor.GOLD + "Disco-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 7) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.YELLOW);
        ItemStack color = ItemStackManager.createBoots(ChatColor.AQUA + "Color-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 8) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.TEAL);
        ItemStack music = ItemStackManager.createBoots(ChatColor.DARK_GREEN + "Music-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 9) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.GREEN);
        ItemStack lava = ItemStackManager.createBoots(ChatColor.DARK_RED + "Lava-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 10) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.RED);
        ItemStack ender = ItemStackManager.createBoots(ChatColor.DARK_PURPLE + "Ender-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 11) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.PURPLE);
        ItemStack explosive = ItemStackManager.createBoots(ChatColor.DARK_AQUA + "Explosive-Boots",
                (BootsUtil.getBoots(player.getUniqueId().toString(), 12) ? ChatColor.GREEN + "Gekauft!"
                        : ChatColor.RED + "200 Tokens"),
                1, Color.GRAY);
        ItemStack back = ItemStackManager.createItem(ChatColor.GRAY + "Zum Shop", null, Material.BARRIER, 1, 0);
        ItemStack remove = ItemStackManager.createItem(ChatColor.RED + "Boots entfernen", null, Material.LEATHER_BOOTS, 1, 0);

        inventory.setItem(0, love);
        inventory.setItem(1, fire);
        inventory.setItem(2, smoke);
        inventory.setItem(3, angry);
        inventory.setItem(4, cloud);
        inventory.setItem(5, cookie);
        inventory.setItem(6, disco);
        inventory.setItem(7, color);
        inventory.setItem(8, music);
        inventory.setItem(9, lava);
        inventory.setItem(10, ender);
        inventory.setItem(11, explosive);
        inventory.setItem(16, back);
        inventory.setItem(17, remove);

        player.openInventory(inventory);
    }

}
