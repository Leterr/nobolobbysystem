package net.noboplay.lobbysystem.boots;

import net.noboplay.core.api.Tokens;
import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.util.*;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryClickEventBoots implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        try {
            if (event.getInventory().getName().equalsIgnoreCase(ChatColor.AQUA + "Boots")) {
                if (event.getCurrentItem() != null) {
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.RED + "Love-Boots")) {
                        if (LobbySystem.getInstance().discoplayer.contains(player)) {
                            LobbySystem.getInstance().discoplayer.remove(player);
                        }
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 1)) {
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.RED + "Love-Boots", null, 1, Color.PURPLE));
                        } else {
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                    LobbySystem.getInstance().discoplayer.remove(player);
                                }
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 1);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.RED + "Love-Boots", null, 1, Color.PURPLE));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Fire-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 2)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.DARK_RED + "Fire-Boots", null, 1, Color.RED));
                        } else {
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                    LobbySystem.getInstance().discoplayer.remove(player);
                                }
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 2);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.DARK_RED + "Fire-Boots", null, 1, Color.RED));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GRAY + "Smoke-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 3)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.DARK_GRAY + "Smoke-Boots", null, 1, Color.GRAY));
                        } else {
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                    LobbySystem.getInstance().discoplayer.remove(player);
                                }
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 3);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.DARK_GRAY + "Smoke-Boots", null, 1, Color.GRAY));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Angry-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 4)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.DARK_RED + "Angry-Boots", null, 1, Color.RED));
                        } else {
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                    LobbySystem.getInstance().discoplayer.remove(player);
                                }
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 4);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.DARK_RED + "Angry-Boots", null, 1, Color.RED));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "Cloud-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 5)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.GRAY + "Cloud-Boots", null, 1, Color.WHITE));
                        } else {
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                    LobbySystem.getInstance().discoplayer.remove(player);
                                }
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 5);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.GRAY + "Cloud-Boots", null, 1, Color.WHITE));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Keks-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 6)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.GOLD + "Keks-Boots", null, 1, Color.FUCHSIA));
                        } else {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 6);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.GOLD + "Keks-Boots", null, 1, Color.FUCHSIA));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Disco-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 7)) {
                            LobbySystem.getInstance().discoplayer.add(player);
                        } else {
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 7);
                                ScoreboardUtil.setScoreboard(player);
                                LobbySystem.getInstance().discoplayer.add(player);
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Color-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 8)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.AQUA + "Color-Boots", null, 1, Color.TEAL));
                        } else {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 8);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.AQUA + "Color-Boots", null, 1, Color.TEAL));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GREEN + "Music-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 9)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.DARK_GREEN + "Music-Boots", null, 1, Color.GREEN));
                        } else {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 9);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.DARK_GREEN + "Music-Boots", null, 1, Color.GREEN));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "Lava-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 10)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.DARK_RED + "Lava-Boots", null, 1, Color.RED));
                        } else {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 10);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.DARK_RED + "Lava-Boots", null, 1, Color.RED));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }

                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + "Ender-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 11)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.DARK_PURPLE + "Ender-Boots", null, 1, Color.PURPLE));
                        } else {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 11);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory()
                                        .setBoots(ItemStackManager.createBoots(ChatColor.DARK_PURPLE + "Ender-Boots", null, 1, Color.PURPLE));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_GRAY + "Explosive-Boots")) {
                        player.closeInventory();
                        if (BootsUtil.getBoots(player.getUniqueId().toString(), 12)) {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            player.getInventory()
                                    .setBoots(ItemStackManager.createBoots(ChatColor.DARK_GRAY + "Explosive-Boots", null, 1, Color.GRAY));
                        } else {
                            if (LobbySystem.getInstance().discoplayer.contains(player)) {
                                LobbySystem.getInstance().discoplayer.remove(player);
                            }
                            if (Tokens.getTokens(player.getUniqueId().toString()) >= 200) {
                                Tokens.removeTokens(player.getUniqueId().toString(), 200);
                                BootsUtil.giveBoots(player.getUniqueId().toString(), 12);
                                ScoreboardUtil.setScoreboard(player);
                                player.getInventory().setBoots(
                                        ItemStackManager.createBoots(ChatColor.DARK_GRAY + "Explosive-Boots", null, 1, Color.GRAY));
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Du hast die Boots gekauft!");
                            } else {
                                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Du hast nicht gen" + Variables.CHAR_UE + "gend Tokens!");
                            }
                        }
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GRAY + "Zum Shop")) {
                        player.closeInventory();
                        Shop.openShop(player);
                    }
                    if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.RED + "Boots entfernen")) {
                        player.closeInventory();
                        player.getInventory().setBoots(new ItemStack(Material.AIR));
                        if (LobbySystem.getInstance().discoplayer.contains(player)) {
                            LobbySystem.getInstance().discoplayer.remove(player);
                        }
                    }
                    Settings.setArmor(player.getUniqueId().toString(),
                            InventoryUtil.itemStackArrayToBase64(player.getInventory().getArmorContents()));
                }
            }
        } catch (Exception e) {
        }
    }

}
