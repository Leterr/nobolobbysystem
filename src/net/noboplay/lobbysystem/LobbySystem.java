package net.noboplay.lobbysystem;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.noboplay.lobbysystem.boots.DiscoBoots;
import net.noboplay.lobbysystem.boots.InventoryClickEventBoots;
import net.noboplay.lobbysystem.commands.*;
import net.noboplay.lobbysystem.itemmanager.*;
import net.noboplay.lobbysystem.listener.*;
import net.noboplay.lobbysystem.util.ActionBarCountdown;
import net.noboplay.lobbysystem.util.MySQL;
import net.noboplay.lobbysystem.util.SpawnLocation;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class LobbySystem extends JavaPlugin {

    /**
     * Coded by Leterr & Zocker_SK [NoboPlay.net] 2015
     */

	/* Integer */
    public int playercount = 0;
    public int maxplayers = 1;

    /* Instanz */
    private static LobbySystem instance;

    /* String */
    public final String PREFIX = ChatColor.AQUA + "NoboPlay " + ChatColor.DARK_GRAY + Variables.CHAR_DOUBLE_ARROWS + " ";
    public final String NOPERM = ChatColor.RED + "Keine Rechte.";

    /* HashMap */
    public HashMap<String, BukkitRunnable> schild = new HashMap<>();
    public HashMap<String, BukkitRunnable> jumper = new HashMap<>();

    /* ArrayList */
    public ArrayList<String> build = new ArrayList<>();
    public ArrayList<Player> discoplayer = new ArrayList<>();

    /* Cache */
    public Cache<String, String> boots = CacheBuilder.newBuilder().concurrencyLevel(4).weakKeys()
            .expireAfterWrite(5, TimeUnit.SECONDS).build();
    public Cache<String, String> pearl = CacheBuilder.newBuilder().concurrencyLevel(4).weakKeys()
            .expireAfterWrite(3, TimeUnit.SECONDS).build();
    public Cache<String, String> tnt = CacheBuilder.newBuilder().concurrencyLevel(4).weakKeys()
            .expireAfterWrite(5, TimeUnit.SECONDS).build();
    public Cache<String, String> cookie = CacheBuilder.newBuilder().concurrencyLevel(4).weakKeys().expireAfterWrite(500, TimeUnit.MILLISECONDS).build();

    @Override
    public void onEnable() {
        instance = this;
        registerListener(getServer().getPluginManager());
        registerCommands();
        registerBungeeCord();
        setupWorld();
        ActionBarCountdown.startCountdown();
        DiscoBoots.startThread();
        MySQL.createFile();
        MySQL.connect();
        initMySQL();
        PlayerMoveListener.startScheduler();
        System.out.println("NoboPlay " + Variables.CHAR_DOUBLE_ARROWS + " Plugin geladen!");
    }

    @Override
    public void onDisable() {
        MySQL.close();
        System.out.println("NoboPlay " + Variables.CHAR_DOUBLE_ARROWS + " Plugin deaktiviert!");
    }

    private void registerListener(PluginManager pluginmanager) {
        // itemmanager
        pluginmanager.registerEvents(new ProfilManager(), this);
        pluginmanager.registerEvents(new NavigatorManager(), this);
        pluginmanager.registerEvents(new SchildManager(), this);
        pluginmanager.registerEvents(new SilenthubManager(), this);
        pluginmanager.registerEvents(new ShopManager(), this);
        pluginmanager.registerEvents(new GadgetsManager(), this);
        pluginmanager.registerEvents(new FlystickManager(), this);
        pluginmanager.registerEvents(new TrollTNTManager(), this);
        pluginmanager.registerEvents(new ClothingManager(), this);
        pluginmanager.registerEvents(new ConfigManager(), this);
        pluginmanager.registerEvents(new RulesManager(), this);
        // listener
        pluginmanager.registerEvents(new AsyncPreLoginListener(), this);
        pluginmanager.registerEvents(new BlockBreakListener(), this);
        pluginmanager.registerEvents(new BlockPlaceListener(), this);
        pluginmanager.registerEvents(new BrewListener(), this);
        pluginmanager.registerEvents(new CreatureSpawnListener(), this);
        pluginmanager.registerEvents(new EntityDamageListener(), this);
        pluginmanager.registerEvents(new EntityExplodeListener(), this);
        pluginmanager.registerEvents(new InventoryClickListener(), this);
        pluginmanager.registerEvents(new FoodLevelChangeListener(), this);
        pluginmanager.registerEvents(new PlayerCommandPreProcessListener(), this);
        pluginmanager.registerEvents(new PlayerDeathListener(), this);
        pluginmanager.registerEvents(new PlayerDropItemListener(), this);
        pluginmanager.registerEvents(new PlayerEnterBedListener(), this);
        pluginmanager.registerEvents(new PlayerEnterPortalListener(), this);
        pluginmanager.registerEvents(new PlayerInteractListener(), this);
        pluginmanager.registerEvents(new PlayerJoinListener(), this);
        pluginmanager.registerEvents(new PlayerLoginListener(), this);
        pluginmanager.registerEvents(new PlayerMoveListener(), this);
        pluginmanager.registerEvents(new PlayerPickupItemListener(), this);
        pluginmanager.registerEvents(new PlayerQuitListener(), this);
        pluginmanager.registerEvents(new SignChangeListener(), this);
        pluginmanager.registerEvents(new WeatherChangeListener(), this);
        pluginmanager.registerEvents(new EntityShootBowListener(), this);
        pluginmanager.registerEvents(new ProjectileHitListener(), this);
        // BootsListener
        pluginmanager.registerEvents(new InventoryClickEventBoots(), this);
    }

    private void registerCommands() {
        getCommand("build").setExecutor(new BuildCommand());
        getCommand("set").setExecutor(new SetCommand());
        getCommand("fly").setExecutor(new FlyCommand());
        getCommand("spawn").setExecutor(new SpawnCommand());
        getCommand("boots").setExecutor(new BootsCommand());
        getCommand("head").setExecutor(new HeadCommand());
        getCommand("prem").setExecutor(new PremCommand());
        getCommand("tp").setExecutor(new TpCommand());
    }

    private void registerBungeeCord() {
        getServer().getMessenger().registerOutgoingPluginChannel(this, "NoboBungee");
        getServer().getMessenger().registerIncomingPluginChannel(LobbySystem.getInstance(), "lobbysystem",
                new PluginMessageReceivedListener());
    }

    private void setupWorld() {
        SpawnLocation.getMitte().getWorld().setTime(1000);
        SpawnLocation.getMitte().getWorld().getWorldBorder().setCenter(SpawnLocation.getMitte());
        SpawnLocation.getMitte().getWorld().getWorldBorder().setSize(500);
    }

    public static LobbySystem getInstance() {
        return instance;
    }

    private void initMySQL() {
        MySQL.connect();
        try {
            MySQL.getConnection()
                    .prepareStatement(
                            "CREATE TABLE IF NOT EXISTS Boots(UUID varchar(64), boot1 bool, boot2 bool, boot3 bool, boot4 bool, boot5 bool, boot6 bool, boot7 bool, boot8 bool, boot9 bool, boot10 bool, boot11 bool, boot12 bool, UNIQUE (UUID));")
                    .executeUpdate();
            MySQL.getConnection()
                    .prepareStatement(
                            "CREATE TABLE IF NOT EXISTS Rules(UUID varchar(64), accepted bool, UNIQUE (UUID));")
                    .executeUpdate();
            MySQL.getConnection()
                    .prepareStatement(
                            "CREATE TABLE IF NOT EXISTS Settings(UUID varchar(64), doublejump bool, fly bool, hidden bool, speed bool, flystick bool, armor LONGTEXT, UNiQUE(UUID));")
                    .executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
