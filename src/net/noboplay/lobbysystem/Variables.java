package net.noboplay.lobbysystem;

import java.util.Random;

/**
 * Created by steven on 26.03.16.
 */
public class Variables {

    public static final Random RANDOM = new Random();
    public static final String CHAR_DOUBLE_ARROWS = "»";
    public static final String CHAR_AE = "ä";
    public static final String CHAR_OE = "ö";
    public static final String CHAR_UE = "ü";
    public static final String CHAR_SZ = "ß";

}
