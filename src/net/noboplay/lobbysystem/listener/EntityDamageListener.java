package net.noboplay.lobbysystem.listener;

import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import net.noboplay.lobbysystem.util.SpawnLocation;

public class EntityDamageListener implements Listener {

	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			event.setCancelled(true);
			if (player.getFireTicks() > 0) {
				player.setFireTicks(0);
			}
			if (event.getCause() == DamageCause.VOID) {
				SpawnLocation.teleportspawn(player);
			}
		} else if (event.getEntity() instanceof EnderPearl) {
			event.getEntity().remove();
		}
	}

}
