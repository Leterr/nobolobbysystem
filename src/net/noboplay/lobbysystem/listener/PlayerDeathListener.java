package net.noboplay.lobbysystem.listener;

import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        event.setDeathMessage(null);
        event.setDroppedExp(0);
        event.getDrops().clear();
        player.spigot().respawn();
        player.kickPlayer(ChatColor.RED + "Sorry, etwas ist schief gelaufen, die Admins sind informiert!\n" + ChatColor.GREEN + "Bitte joine neu!");
        for (Player players : Bukkit.getOnlinePlayers()) {
            if (players.hasPermission("server.admin")) {
                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + player.getName()
                        + ChatColor.RED + " ist gestorben, bitte " + Variables.CHAR_UE + "berpr" + Variables.CHAR_UE + "fe das LobbySystem!");
            }
        }
    }

}
