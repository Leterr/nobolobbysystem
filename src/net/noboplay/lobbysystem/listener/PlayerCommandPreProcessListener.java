package net.noboplay.lobbysystem.listener;

import net.noboplay.core.api.Nametags;
import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.help.HelpTopic;

public class PlayerCommandPreProcessListener implements Listener {

    @EventHandler
    public void onPreprocess(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        if (event.getMessage().equalsIgnoreCase("/reload") || event.getMessage().equalsIgnoreCase("/rl")) {
            if (player.hasPermission("server.admin")) {
                Bukkit.broadcastMessage(LobbySystem.getInstance().PREFIX + ChatColor.DARK_RED + "Alle Daten werden neugeladen!");
                Bukkit.reload();
                Bukkit.broadcastMessage(LobbySystem.getInstance().PREFIX + ChatColor.GREEN + "Alle Daten wurden neugeladen!");
                Bukkit.getOnlinePlayers().forEach(Nametags::setDisplayName);
            } else {
                player.sendMessage(LobbySystem.getInstance().PREFIX + LobbySystem.getInstance().NOPERM);
            }
        }
        if (!event.isCancelled()) {
            String msg = event.getMessage().split(" ")[0];
            HelpTopic topic = Bukkit.getServer().getHelpMap().getHelpTopic(msg);
            if (topic == null) {
                player.sendMessage(LobbySystem.getInstance().PREFIX + ChatColor.RED + "Dieser Command existiert nicht " + ChatColor.DARK_GRAY + Variables.CHAR_DOUBLE_ARROWS + ChatColor.DARK_AQUA + " /help");
                event.setCancelled(true);
            }
        }
    }

}
