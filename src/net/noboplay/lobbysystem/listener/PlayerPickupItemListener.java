package net.noboplay.lobbysystem.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import net.noboplay.lobbysystem.LobbySystem;

public class PlayerPickupItemListener implements Listener {

	@EventHandler
	public void onPickUp(PlayerPickupItemEvent event) {
		Player player = event.getPlayer();
		if (LobbySystem.getInstance().build.contains(player.getName())) {
			return;
		}
		event.setCancelled(true);
	}

}
