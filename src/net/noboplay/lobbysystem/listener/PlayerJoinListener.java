package net.noboplay.lobbysystem.listener;

import net.noboplay.core.api.Nametags;
import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.util.*;
import net.noboplay.lobbysystem.util.Settings.SettingsOption;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        event.setJoinMessage(null);
        player.getInventory().setArmorContents(null);
        player.getInventory().clear();
        player.setHealth(20.0D);
        player.setFoodLevel(20);
        player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 55);
        if (!player.hasPlayedBefore()) {
            SpawnLocation.teleportspawn(player);
        }
        ScoreboardUtil.setScoreboard(player);
        ScoreboardUtil.updateNametag(player);
        LabyModFeatures.setLabyModFeature(player, LabyModFeatures.getFeatureList());
        Bukkit.getScheduler().runTaskAsynchronously(LobbySystem.getInstance(), () -> {
            PlayerJoinItems.joinItems(player);
            Nametags.setDisplayName(player);
            Bukkit.getOnlinePlayers().forEach(ScoreboardUtil::updateNametag);
            if (!Settings.isPlayerInSQL(player.getUniqueId().toString()))
                Settings.createPlayer(player.getUniqueId().toString());
            if (Settings.getArmor(player.getUniqueId().toString()) != null)
                player.getInventory().setArmorContents(InventoryUtil.itemStackArrayFromBase64(Settings.getArmor(player.getUniqueId().toString())));
            for (Player players : Bukkit.getOnlinePlayers()) {
                if (Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.HIDDEN))
                    if (!players.hasPermission("server.team"))
                        player.hidePlayer(players);
                if (Settings.getBoolean(players.getUniqueId().toString(), SettingsOption.HIDDEN))
                    players.hidePlayer(player);
            }
            if ((Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.DOUBLEJUMP))
                    || (Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.FLY))) {
                player.setAllowFlight(true);
                player.setFlying(true);
            }
            if (!BootsUtil.playerExists(player.getUniqueId().toString())) {
                BootsUtil.createPlayer(player.getUniqueId().toString());
            }
        });
        if (!RulesUtil.isPlayerInSQL(player.getUniqueId().toString()))
            new BukkitRunnable() {

                @Override
                public void run() {
                    RulesUtil.openRules(player);
                }
            }.runTaskLaterAsynchronously(LobbySystem.getInstance(), 20);
    }
}
