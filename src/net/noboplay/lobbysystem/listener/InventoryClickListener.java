package net.noboplay.lobbysystem.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.noboplay.lobbysystem.LobbySystem;

public class InventoryClickListener implements Listener {
	
	@EventHandler
	public void onClick(InventoryClickEvent event) {
	    Player player = (Player) event.getWhoClicked();
		if(LobbySystem.getInstance().build.contains(player.getName())) {
			return;
		}
		event.setCancelled(true);
	}

}
