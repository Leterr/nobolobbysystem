package net.noboplay.lobbysystem.listener;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import net.noboplay.lobbysystem.LobbySystem;

public class PluginMessageReceivedListener implements PluginMessageListener {

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if (!channel.equals("LobbySystem")) {
			return;
		}
		ByteArrayDataInput in = ByteStreams.newDataInput(message);
		if (in.readUTF().equalsIgnoreCase("GetOnlinePlayer"))
			LobbySystem.getInstance().playercount = in.readInt();
		else if (in.readUTF().equalsIgnoreCase("GetMaxPlayer"))
			LobbySystem.getInstance().maxplayers = in.readInt();
	}

}
