package net.noboplay.lobbysystem.listener;

import net.noboplay.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        if (event.getAction() == Action.PHYSICAL && event.getClickedBlock().getType() == Material.SOIL) {
            event.setCancelled(true);
        }
        if (event.getItem() != null) {
            if (event.getItem().getType() == Material.BOW
                    && (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)) {
                event.getPlayer().getInventory().setItem(10, new ItemStack(Material.ARROW));
            }
        }
        if (event.getClickedBlock() != null) {
            if (event.getAction() == Action.PHYSICAL) {
                if (event.getClickedBlock().getType() == Material.WOOD_PLATE) {
                    player.getWorld().playEffect(player.getEyeLocation(), Effect.FIREWORKS_SPARK, 20);
                    player.playSound(player.getLocation(), Sound.FIREWORK_BLAST, 1, 1);
                    Bukkit.getScheduler().runTaskLaterAsynchronously(LobbySystem.getInstance(), () -> {
                        player.setVelocity(new Vector(0, 0.9D, 0));
                        Vector v1 = player.getLocation().getDirection().multiply(10.3D).setY(1.3D);
                        player.playSound(player.getLocation(), Sound.FIREWORK_BLAST2, 1, 1);
                        player.setVelocity(v1);
                    }, 5L);
                }
                if (event.getClickedBlock().getType() == Material.GOLD_PLATE) {
                    Bukkit.getScheduler().runTaskLaterAsynchronously(LobbySystem.getInstance(), () -> player.setVelocity(new Vector(0, 2, 0)), 5);
                    player.getWorld().playEffect(player.getEyeLocation(), Effect.FIREWORKS_SPARK, 20);
                    player.playSound(player.getLocation(), Sound.FIREWORK_BLAST, 1, 1);
                    Bukkit.getScheduler().runTaskLaterAsynchronously(LobbySystem.getInstance(), () -> {
                        Vector v1 = player.getLocation().getDirection().multiply(10.0D).setY(0.3D);
                        player.playSound(player.getLocation(), Sound.FIREWORK_BLAST2, 1, 1);
                        player.setVelocity(v1);
                    }, 10L);
                }
            }
        }
    }
}
