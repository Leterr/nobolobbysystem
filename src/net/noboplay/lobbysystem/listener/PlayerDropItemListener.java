package net.noboplay.lobbysystem.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import net.noboplay.lobbysystem.LobbySystem;

public class PlayerDropItemListener implements Listener {

	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
		if (LobbySystem.getInstance().build.contains(player.getName())) {
			return;
		}
		event.setCancelled(true);
	}

}
