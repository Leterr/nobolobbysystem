package net.noboplay.lobbysystem.listener;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class SignChangeListener implements Listener {

	@EventHandler
	public void onChange(SignChangeEvent event) {
		for (int i = 0; i <= 3; i++) {
			String line = event.getLine(i);
			line = ChatColor.translateAlternateColorCodes('&', line);
			event.setLine(i, line);
		}
	}

}