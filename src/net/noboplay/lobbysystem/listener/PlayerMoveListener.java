package net.noboplay.lobbysystem.listener;

import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;
import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.Variables;
import net.noboplay.lobbysystem.util.Settings;
import net.noboplay.lobbysystem.util.Settings.SettingsOption;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.HashSet;

public class PlayerMoveListener implements Listener {

    public static void startScheduler() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(LobbySystem.getInstance(), new Runnable() {

            @Override
            public void run() {
                for (final Player player : Bukkit.getOnlinePlayers()) {
                    final Location[] enderlocation = new Location[1];
                    try {
                        enderlocation[0] = player.getTargetBlock((HashSet<Material>) null, 150).getLocation();
                    } catch (Exception e) {
                    }
                    if (player.getLocation().getBlock().getType() == Material.WATER
                            || player.getLocation().getBlock().getType() == Material.STATIONARY_WATER && !Settings
                            .getBoolean(player.getUniqueId().toString(), SettingsOption.DOUBLEJUMP)) {
                        if (player.isSneaking()) {
                            player.setVelocity(player.getLocation().getDirection().multiply(2));
                        } else {
                            player.setVelocity(new Vector(0, 0.9, 0));
                            Vector vector = null;
                            switch (Variables.RANDOM.nextInt(2)) {
                                case 0:
                                    vector = player.getLocation().getDirection().multiply(0).setY(1);
                                    break;
                                case 1:
                                    vector = player.getLocation().getDirection().multiply(0).setY(2);
                                    break;
                                case 2:
                                    vector = player.getLocation().getDirection().multiply(0).setY(3);
                                    break;
                            }
                            player.setVelocity(vector);
                        }
                    }
                    if (player.isFlying()
                            && Settings.getBoolean(player.getUniqueId().toString(), SettingsOption.DOUBLEJUMP)) {
                        Bukkit.getScheduler().runTask(LobbySystem.getInstance(), () -> player.setAllowFlight(false));
                        player.setVelocity(player.getLocation().getDirection().multiply(3.0).setY(1.0));
                        player.playSound(player.getLocation(), Sound.VILLAGER_YES, 1.5F, 1.9F);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(LobbySystem.getInstance(), () -> player.setAllowFlight(true), 25);
                    }
                    if (player.getInventory().getBoots() != null) {
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.DARK_RED + "Fire-Boots")) {
                            if (player.isSneaking()) {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.FLAME, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 1);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            } else {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.FLAME, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 2);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            }
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.RED + "Love-Boots")) {
                            if (player.isSneaking()) {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.HEART, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 2);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            } else {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.HEART, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 5);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            }
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.DARK_GRAY + "Smoke-Boots")) {
                            if (player.isSneaking()) {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.SMOKE_NORMAL, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 1);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            } else {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.SMOKE_NORMAL, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 2);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            }
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.DARK_RED + "Angry-Boots")) {
                            if (player.isSneaking()) {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.VILLAGER_ANGRY, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 2);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            } else {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.VILLAGER_ANGRY, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 2);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            }
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.GRAY + "Cloud-Boots")) {
                            if (player.isSneaking()) {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.CLOUD, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 2);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                                if (LobbySystem.getInstance().boots.getIfPresent(player.getName()) != null)
                                    return;
                                player.setVelocity(player.getLocation().getDirection().setY(5));
                                LobbySystem.getInstance().boots.put(player.getName(), player.getName());
                            } else {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.CLOUD, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 5);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            }
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.GOLD + "Keks-Boots")) {
                            if (LobbySystem.getInstance().cookie.getIfPresent(player.getName()) != null)
                                return;
                            final Item[] cookie = new Item[1];
                            LobbySystem.getInstance().cookie.put(player.getName(), player.getName());
                            PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                    EnumParticle.VILLAGER_HAPPY, true, (float) player.getLocation().getX(),
                                    (float) player.getLocation().getY(), (float) player.getLocation().getZ(), (float) 1,
                                    (float) 1, (float) 1, 1, 2);
                            for (Player players : player.getWorld().getPlayers())
                                ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            Bukkit.getScheduler().runTask(LobbySystem.getInstance(), () -> cookie[0] = player.getWorld().dropItem(player.getLocation(),
                                    new ItemStack(Material.COOKIE)));
                            Bukkit.getScheduler().runTaskLaterAsynchronously(LobbySystem.getInstance(), () -> cookie[0].remove(), 60);
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.AQUA + "Color-Boots")) {
                            PacketPlayOutWorldParticles p = new PacketPlayOutWorldParticles(EnumParticle.SPELL_MOB,
                                    true, (float) player.getLocation().getX(),
                                    (float) player.getLocation().getY() - 0.5F, (float) player.getLocation().getZ(),
                                    (float) 1, (float) 1, (float) 1, 1, 4);
                            PacketPlayOutWorldParticles p1 = new PacketPlayOutWorldParticles(EnumParticle.SPELL, true,
                                    (float) player.getLocation().getX(), (float) player.getLocation().getY() - 0.5F,
                                    (float) player.getLocation().getZ(), (float) 1, (float) 1, (float) 1, 1, 4);
                            PacketPlayOutWorldParticles p2 = new PacketPlayOutWorldParticles(
                                    EnumParticle.SPELL_MOB_AMBIENT, true, (float) player.getLocation().getX(),
                                    (float) player.getLocation().getY() - 0.5F, (float) player.getLocation().getZ(),
                                    (float) 1, (float) 1, (float) 1, 1, 4);
                            for (Player all : player.getWorld().getPlayers()) {
                                ((CraftPlayer) all).getHandle().playerConnection.sendPacket(p);
                                ((CraftPlayer) all).getHandle().playerConnection.sendPacket(p1);
                                ((CraftPlayer) all).getHandle().playerConnection.sendPacket(p2);
                            }
                            PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                    EnumParticle.CRIT_MAGIC, true, (float) player.getLocation().getX(),
                                    (float) player.getLocation().getY(), (float) player.getLocation().getZ(), (float) 1,
                                    (float) 1, (float) 1, 1, 1);
                            for (Player players : player.getWorld().getPlayers())
                                ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.DARK_GREEN + "Music-Boots")) {
                            if (player.isSneaking()) {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.NOTE, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 1);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 2);
                                player.playSound(player.getLocation(), Sound.NOTE_BASS_GUITAR, 1, 2);
                                player.playSound(player.getLocation(), Sound.NOTE_PIANO, 1, 2);
                            } else {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.NOTE, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 2);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            }
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.DARK_RED + "Lava-Boots")) {
                            if (player.isSneaking()) {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.LAVA, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 4);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            } else {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.LAVA, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 2);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            }
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.DARK_PURPLE + "Ender-Boots")) {
                            if (player.isSneaking()) {
                                if (LobbySystem.getInstance().boots.getIfPresent(player.getName()) != null)
                                    return;
                                Bukkit.getScheduler().runTask(LobbySystem.getInstance(), () -> {
                                    player.teleport(enderlocation[0].clone().add(0, 1.5, 0));
                                    player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
                                    player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 1));
                                });
                                LobbySystem.getInstance().boots.put(player.getName(), player.getName());
                            } else {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.PORTAL, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 5);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            }
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.DARK_GRAY + "Explosive-Boots")) {
                            if (player.isSneaking()) {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.EXPLOSION_NORMAL, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 2);
                                player.playSound(player.getLocation(), Sound.EXPLODE, 1, 1);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            } else {
                                PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                        EnumParticle.EXPLOSION_NORMAL, true, (float) player.getLocation().getX(),
                                        (float) player.getLocation().getY(), (float) player.getLocation().getZ(),
                                        (float) 1, (float) 1, (float) 1, 1, 1);
                                for (Player players : player.getWorld().getPlayers())
                                    ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                            }
                        }
                        if (player.getInventory().getBoots().getItemMeta().getDisplayName()
                                .equalsIgnoreCase(ChatColor.GOLD + "Disco-Boots")) {
                            PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(
                                    EnumParticle.REDSTONE, true, (float) player.getLocation().getX(),
                                    (float) player.getLocation().getY(), (float) player.getLocation().getZ(), (float) 1,
                                    (float) 1, (float) 2, 3, 4);
                            for (Player players : player.getWorld().getPlayers())
                                ((CraftPlayer) players).getHandle().playerConnection.sendPacket(particles);
                        }
                    }
                }
            }
        }, 0, 3);
    }
}