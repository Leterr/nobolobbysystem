package net.noboplay.lobbysystem.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.LeavesDecayEvent;

import net.noboplay.lobbysystem.LobbySystem;

public class BlockBreakListener implements Listener {

	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (LobbySystem.getInstance().build.contains(player.getName())) {
			return;
		}
		event.setCancelled(true);
	}

	@EventHandler
	public void onLeave(LeavesDecayEvent event) {
		event.setCancelled(true);
	}

}
