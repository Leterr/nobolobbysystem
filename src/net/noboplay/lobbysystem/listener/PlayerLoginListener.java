package net.noboplay.lobbysystem.listener;

import net.noboplay.core.api.Core;
import net.noboplay.lobbysystem.LobbySystem;
import net.noboplay.lobbysystem.Variables;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class PlayerLoginListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();
        if (Core.getServerName() != null) {
            if (Core.getServerName().toLowerCase().startsWith("silenthub")) {
                if (player.hasPermission("server.team") || player.hasPermission("server.youtuber")) {
                    event.allow();
                } else {
                    event.disallow(Result.KICK_OTHER,
                            LobbySystem.getInstance().PREFIX + ChatColor.DARK_RED + "Keine Berechtigung f" + Variables.CHAR_UE + "r diesen Server.");
                }
                return;
            }
        }
        if (Bukkit.getOnlinePlayers().size() >= Bukkit.getMaxPlayers()) {
            if (player.hasPermission("server.premium") && player.hasPermission("server.team")) {
                event.allow();
            } else {
                event.disallow(Result.KICK_OTHER,
                        ChatColor.RED + "Der Server ist voll!\n" + ChatColor.GOLD + "Kaufe dir Premium um volle Server zu betreten!\nhttp://store.noboplay.net/");
            }
        }
        if (LobbySystem.getInstance().playercount >= LobbySystem.getInstance().maxplayers
                && !player.hasPermission("server.premium")) {
            event.disallow(Result.KICK_OTHER,
                    ChatColor.RED + "Das Netzwerk ist voll!\n" + ChatColor.GOLD + "Kaufe dir Premium um volle Server zu betreten!\nhttp://store.noboplay.net/");
        }
    }

}
