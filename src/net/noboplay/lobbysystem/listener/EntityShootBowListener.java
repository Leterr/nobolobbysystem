package net.noboplay.lobbysystem.listener;

import net.noboplay.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.util.Vector;

public class EntityShootBowListener implements Listener {

    @EventHandler
    public void onBowShoot(EntityShootBowEvent event) {
        if (event.getEntity() instanceof Player) {
            final Player player = (Player) event.getEntity();
            if (event.getBow().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + "EnderBow")) {
                event.getProjectile().remove();
                if (LobbySystem.getInstance().pearl.asMap().containsKey(player.getName())) {
                    player.sendMessage(
                            LobbySystem.getInstance().PREFIX + ChatColor.RED + "Bitte warte, bis deine Enderperle angekommen ist!");
                    return;
                }
                Vector vec = event.getProjectile().getVelocity();
                EnderPearl pearl = event.getEntity().launchProjectile(EnderPearl.class);
                pearl.setVelocity(vec);
                LobbySystem.getInstance().pearl.put(player.getName(), player.getName());
                pearl.setShooter(player);
                pearl.setPassenger(player);
                Bukkit.getScheduler().runTaskLaterAsynchronously(LobbySystem.getInstance(), () -> {
                    if (pearl != null) pearl.remove();
                }, 100);
            }
        }
    }

}
