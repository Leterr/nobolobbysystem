package net.noboplay.lobbysystem.listener;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.noboplay.lobbysystem.LobbySystem;

public class AsyncPreLoginListener implements Listener {

	@EventHandler
	public void onLogin(AsyncPlayerPreLoginEvent event) {
		if (Bukkit.getOnlinePlayers().size() > 0) {
			getProxyPlayers();
			getMaxProxyPlayers();
		}
	}

	private void getProxyPlayers() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("GetOnlinePlayer");
		Iterables.getFirst(Bukkit.getOnlinePlayers(), null).sendPluginMessage(LobbySystem.getInstance(), "NoboBungee",
				out.toByteArray());
	}

	private void getMaxProxyPlayers() {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("GetMaxPlayer");
		Iterables.getFirst(Bukkit.getOnlinePlayers(), null).sendPluginMessage(LobbySystem.getInstance(), "NoboBungee",
				out.toByteArray());
	}

}
