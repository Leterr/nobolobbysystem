package net.noboplay.lobbysystem.listener;

import net.noboplay.lobbysystem.LobbySystem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        event.setQuitMessage(null);
        try {
            LobbySystem.getInstance().schild.get(player.getName()).cancel();
            LobbySystem.getInstance().schild.remove(player.getName());
            LobbySystem.getInstance().discoplayer.remove(player);
        } catch (Exception exception) {
        }
    }

}
