package net.noboplay.lobbysystem.listener;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

public class ProjectileHitListener implements Listener {

	@EventHandler
	public void onHit(ProjectileHitEvent event) {
		if (event.getEntity() instanceof EnderPearl) {
			EnderPearl pearl = (EnderPearl) event.getEntity();
			if (!(pearl.getShooter() instanceof Player)) {
				return;
			}
			final Player player = (Player) pearl.getShooter();
			Location pl = player.getLocation();
			double px = pl.getX();
			double py = pl.getY();
			double pz = pl.getZ();
			f: for (int i = 0; i < 255; i++) {
				Location checkx = new Location(player.getWorld(), px + 1.0D, py + 3.0D + i, pz,
						player.getLocation().getYaw(), player.getLocation().getPitch());
				Location checky = new Location(player.getWorld(), px, py + 3.0D + i, pz + 1.0D,
						player.getLocation().getYaw(), player.getLocation().getPitch());
				Location checkx1 = new Location(player.getWorld(), px - 1.0D, py + 3.0D + i, pz,
						player.getLocation().getYaw(), player.getLocation().getPitch());
				Location checky1 = new Location(player.getWorld(), px, py + 3.0D + i, pz - 1.0D,
						player.getLocation().getYaw(), player.getLocation().getPitch());
				if (checkx.getBlock().getType() == Material.AIR) {
					player.teleport(checkx);
					break f;
				}
				if (checky.getBlock().getType() == Material.AIR) {
					player.teleport(checky);
					break f;
				}
				if (checkx1.getBlock().getType() == Material.AIR) {
					player.teleport(checkx1);
					break f;
				}
				if (checky1.getBlock().getType() == Material.AIR) {
					player.teleport(checky1);
				}
			}
		}
	}

}
